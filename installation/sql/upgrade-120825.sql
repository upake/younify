ALTER TABLE  `cache_fb_newsfeed` ADD  `post_id` VARCHAR( 50 ) NOT NULL FIRST ,
ADD PRIMARY KEY (  `post_id` );

ALTER TABLE  `cache_fb_newsfeed` ADD  `message` TEXT NOT NULL AFTER  `title`;

ALTER TABLE  `cache_fb_newsfeed` ADD  `description` TEXT NOT NULL AFTER  `message` ,
ADD  `type` VARCHAR( 20 ) NOT NULL AFTER  `description` ,
ADD  `alt` TEXT NOT NULL AFTER  `type`;


ALTER TABLE  `cache_fb_newsfeed` CHANGE  `likes`  `likes` INT( 6 ) NOT NULL ,
CHANGE  `comments`  `comments` INT( 6 ) NOT NULL;

ALTER TABLE  `cache_fb_newsfeed` CHANGE  `post_id`  `post_id` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;