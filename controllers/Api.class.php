<?php
require_once(MODEL_PATH.'User.model.php');
/**
 * LoadHome class
 * 
 * This is the controller class for younify
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */
 
 
class Api extends Controller{

	/**
	* constructor
	*
	*/
	private $nw;
	
	function __construct() {	
		 parent::__construct();
		 $this->fb = new FacebookConnect();
         $this->flickr = new FlickrConnect();
		 $this->instagram = new InstagramConnect();
		 
		 $this->albums = new Albums();
         $this->photos = new Photos();
         $this->feeds = new Feeds();
    }

   /**
    * Controller method to show view
    * 
    *                 	 
    */
	
	public function login()
	{
		$url = $this->fb->getLoginUrl();
		header('Location:'.$url);
	}
	
	public function logout()
	{
		$url = $this->fb->getLogoutUrl();
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_exec ($ch);  
		
		if (curl_errno($ch) == 0) {
			$this->logoutRedirect();
		}
		else
		{
			echo 'Network error :'.curl_error ($ch);   
		}
		
	
	}	
	
	public function loginRedirect()
	{

        $network = $_GET['network'];
		
		if ($network =='FB')
		{
			if (isset($_GET['state'])) {
				$_SESSION['logged_in'] 	= true;
				die();
				$_SESSION['network']	= $network;
				$_SESSION['fb_state'] 	= $_GET['state'];
                $user_id = $this->fb->getUserId();
                $_SESSION['fb_user_id'] = $user_id;

                $this->user = User::getInstance();
                $this->user->add($user_id);
                $_SESSION['user_networks'] = $this->user->getNetworkIds($user_id);

			}
			else
			{
				$_SESSION['logged_in'] 	= false;
				$_SESSION['network'] 	= $network;
				$_SESSION['fb_state'] 	= '';
			}
		}
        else if ($network =='FLICKR')
        {
            $nw_id = $this->flickr->setUserParams();
            $this->user = User::getInstance();
            //$this->user->addNetwork($_SESSION['fb_user_id'], $network,$nw_id)b_user_id'], $network,$nw_id);

        }
		else
		{
			echo 'Invalid entry 1';
		}	

		//load initial view after login
		//$this->showMyAlbums();
	}	
	
	public function logoutRedirect()
	{
		echo 'You have been successfully logged out.<br><br>';
		echo '<a href="http://localhost/younify/index.php"> Return to YOUnify </a>';
			
	}		
	/*
	public function showMyAlbums()
	{
        if (isset($_GET['network']))
            $network = $_GET['network'];
        else
		    $network = $_SESSION['network'];
		
		if ($_SESSION['logged_in']){

            $albumArr = array();

            // get FB albums
            $friendId ='me';
            $last_key = $this->fb->getAlbums($friendId, &$albumArr);
            
            //$nextPage = $obj->paging->next;

            $key = $last_key + 1;

            //get FLICKR photosets, galleries, collections and favourites(1 album)
            $flickr_id = $_SESSION['user_networks']['FLICKR'];
            if (isset($flickr_id))
            {
                $last_key = $this->flickr->getAlbums($flickr_id, &$albumArr, $key);
            }

            include(VIEW_PATH."showAlbum.view.php");
		}
		else{
			$this->login();
		}		
	}*/

    public function showMyAlbums()
    {

        if (isset($_GET['network']))
            $network = $_GET['network'];
        else
            $network = $_SESSION['network'];

        if ($_SESSION['logged_in']){

            $albumArr = array();

            // get FB albums
            $friendId ='me';
            $last_key = $this->fb->getAlbums($friendId, &$albumArr);

            //$nextPage = $obj->paging->next;

            $key = $last_key + 1;

            //get FLICKR photosets, galleries, collections and favourites(1 album)
            $flickr_id = $_SESSION['user_networks']['FLICKR'];
            if (isset($flickr_id))
            {
                $last_key = $this->flickr->getAlbums($flickr_id, &$albumArr, $key);
            }

            //echo "<pre>";
           // print_r($albumArr);

//            foreach($albumArr as $aa) {
//            $a['id'] = $aa['id'];
//            $a['title'] = $aa['name'];
//            $a['image'] = $aa['cover_large'];
//                $a['url'] = $aa['cover_large'];
//                $a['image'] = $aa['cover_large'];
//            $b[] = $a;
//            }
//            echo json_encode($b);
            include(VIEW_PATH."showAlbum.view.php");
        }
        else{
            $this->login();
        }
    }

    function getImages()
    {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        //echo '[{"id":"94080","title":"LETS LIVE","referer":"http:\/\/jakub.tumblr.com\/","url":"http:\/\/www.wookmark.com\/image\/94080\/lets-live","width":"500","height":"646","image":"http:\/\/www.wookmark.com\/images\/original\/94080_tumblr_m4jrvmk5mh1qfwt2xo1_500.jpg","preview":"http:\/\/www.wookmark.com\/images\/thumbs\/94080_tumblr_m4jrvmk5mh1qfwt2xo1_500.jpg"}]';
        echo '[{"id":"10150333912606084","title":"Wall Photos","preview":"http:\/\/a3.sphotos.ak.fbcdn.net\/hphotos-ak-snc7\/s720x720\/432191_10150684814676084_138403810_n.jpg"},{"id":"475765201083","title":"Profile Pictures","preview":"http:\/\/a6.sphotos.ak.fbcdn.net\/hphotos-ak-snc7\/302052_10150340508461084_2535617_n.jpg"},{"id":"10150268166181084","title":"June 11, 2011","preview":"http:\/\/a5.sphotos.ak.fbcdn.net\/hphotos-ak-ash4\/261228_10150268166231084_3880412_n.jpg"},{"id":"491806986083","title":"PicBadges Photos","preview":"http:\/\/a1.sphotos.ak.fbcdn.net\/hphotos-ak-snc6\/75837_491806991083_6677925_n.jpg"},{"id":"404530236083","title":"APIIT Tech team - Diytalawa - Somwhere in 2004","preview":"http:\/\/a7.sphotos.ak.fbcdn.net\/hphotos-ak-prn1\/24258_404530301083_46192_n.jpg"},{"id":"320851541083","title":"Hikkaduwa-Galle-December-2009","preview":"http:\/\/a7.sphotos.ak.fbcdn.net\/hphotos-ak-prn1\/22664_320852131083_1559252_n.jpg"},{"id":"187130341083","title":"Lagos","preview":"http:\/\/a1.sphotos.ak.fbcdn.net\/hphotos-ak-prn1\/14343_187135821083_4723828_n.jpg"},{"id":"8640136083","title":"Random","preview":"http:\/\/a2.sphotos.ak.fbcdn.net\/hphotos-ak-ash4\/150_8640191083_8455_n.jpg"},{"id":"10789236083","title":"wonder years","preview":"http:\/\/a7.sphotos.ak.fbcdn.net\/hphotos-ak-ash4\/173_10789326083_9505_n.jpg"},{"id":"72157628547445041","title":"Architecture","preview":"http:\/\/farm8.staticflickr.com\/7020\/6563273845_c0996e9965.jpg"},{"id":"72157628547484597","title":"Nature","preview":"http:\/\/farm7.staticflickr.com\/6165\/6203025612_7bbf288b6d.jpg"},{"id":"72157627747463398","title":"randoms","preview":"http:\/\/farm7.staticflickr.com\/6166\/6180940290_4d316d3bda.jpg"},{"id":"1520704-72157630124698806","title":"Sydney","preview":"http:\/\/farm8.staticflickr.com\/7006\/6510020299_e9f28841bd.jpg"}]';
    
	}

	function showFriends()
	{
		$friendId = $_GET['friendId'];
		
		$network = $_SESSION['network'];
		
		if ($_SESSION['logged_in']){
			if ($network =='FB')
			{			
				$imgQueryStr = '&network='.$network.'&state='.$_SESSION['fb_state'];
				$this->fb->setUserParams();
				
				$obj = json_decode($this->fb->getFriends());
				
				$nextPage = $obj->paging->next;
				
				$friendCount = count($obj->data);
				$friendArr = array();
				
				foreach($obj->data as $key => $friendObj)
				{
					
					$friendArr[$key]['id']			= $friendObj->id;
					$friendArr[$key]['profilePic']	= $friendObj->picture;
					$friendArr[$key]['name']		= $friendObj->name;
				}
			
				include(VIEW_PATH."friendCanvas.view.php");
			}
			else
			{
				echo 'Invalid entry 3';
			}
		}
		else{

		}		
	}	
	
	
	public function showFriend()
	{
		$friendId = $_GET['friendId'];
		
		$network = $_SESSION['network'];
		
		if ($_SESSION['logged_in']){
			if ($network =='FB')
			{
				$obj = json_decode($this->fb->getAlbums($friendId));
				
				$nextPage = $obj->paging->next;
				
				$albumArr = array();
				
				foreach($obj->data as $key => $albumObj)
				{
					$albumArr[$key]['id']=$albumObj->id;
					
					if (isset($albumObj->cover_photo)){
						$coverObj = json_decode($this->fb->getPhoto($albumObj->cover_photo));
						
						$albumArr[$key]['cover_small'] 	= $coverObj->picture;
						$albumArr[$key]['cover_large']	= $coverObj->source;	
						$albumArr[$key]['cover_height']	= $coverObj->height;	
						$albumArr[$key]['cover_width']	= $coverObj->width;	
					}
					else
					{
						$albumArr[$key]['cover_small'] 	= 'https://graph.facebook.com/'.$friendId.'/picture?small';
						$albumArr[$key]['cover_large']	= 'https://graph.facebook.com/'.$friendId.'/picture?large';
						$albumArr[$key]['cover_height']	= '200';	
						$albumArr[$key]['cover_width']	= '200';						
					}
					
					$albumArr[$key]['name']=$albumObj->name;
					$albumArr[$key]['count']=$albumObj->count;
				}
			
				include(VIEW_PATH."showAlbum.view.php");
			}
			else
			{
				echo 'Invalid entry 4';
			}
		}
		else{
			$this->login();
		}		
	}	
	
	public function showAlbumPics(){
	
		$albumId = $_GET['albumId'];
		
		$network = $_SESSION['network'];
		
		if ($_SESSION['logged_in']){
			if ($network =='FB')
			{
			
				$obj = json_decode($this->fb->getAlbumPhotos($albumId, 'json')); //doesnt work anymore :D
				
				$nextPage = $obj->paging->next;
				
				$photoArr = array();
				
				foreach($obj->data as $key => $pictureObj)
				{
					$photoArr[$key]['id']			= $pictureObj->id;
					$photoArr[$key]['photo_small'] 	= $pictureObj->picture;
					$photoArr[$key]['photo_large']	= $pictureObj->source;	
					$photoArr[$key]['photo_height']	= $pictureObj->height;	
					$photoArr[$key]['photo_width']	= $pictureObj->width;						
				}
			
				 include(VIEW_PATH."showAlbumPhotos.view.php");
			}
			else
			{
				echo 'Invalid entry 5';
			}
		}
		else{
			$this->login();
		}				
	}

    public function getAlbumPics()
    {
        $albumType    = $_GET['albumType'];
        $albumId      = $_GET['id'];
        $returnFormat = $_GET['format'];
        $data          ="";

        if ($albumType == 'FB_ALBUM')
        {
            $fb_arr = $this->fb->getAlbumPhotos($albumId, 'array');

            foreach ($fb_arr['data'] as $key => $value)
            {
                if ($returnFormat =='html')
                {
                    $data = $data.'<div class="albumPicSmall"> <img src="'.$value['picture'].'"/><div class="albumPicLarge"> <img src="'.$value['source'].'"/></div></div>';
                }
            }

        }
        else if ($albumType == 'FLICKR_PHOTOSET')
        {
            $f_sets = $this->flickr->getPhotosetPhotos($albumId, 'array');

            foreach ($f_sets['photoset']['photo'] as $key => $value)
            {
                if ($returnFormat =='html')
                {
                    $picture = "http://farm{$value['farm']}.staticflickr.com/{$value['server']}/{$value['id']}_{$value['secret']}_t.jpg";
                    $source  = "http://farm{$value['farm']}.staticflickr.com/{$value['server']}/{$value['id']}_{$value['secret']}.jpg";
                    $data = $data.'<div class="albumPicSmall"> <img src="'.$picture.'"/><div class="albumPicLarge"> <img src="'.$source.'"/></div></div>';
                }
            }

        }else if ($albumType == 'FLICKR_GALLERY')
        {
            $f_galleries = $this->flickr->getGalleryPhotos($albumId, 'array');

            foreach ($f_galleries['photos']['photo'] as $key => $value)
            {
                if ($returnFormat =='html')
                {
                    $picture = "http://farm{$value['farm']}.staticflickr.com/{$value['server']}/{$value['id']}_{$value['secret']}_t.jpg";
                    $source  = "http://farm{$value['farm']}.staticflickr.com/{$value['server']}/{$value['id']}_{$value['secret']}.jpg";
                    $data = $data.'<div class="albumPicSmall"> <img src="'.$picture.'"/><div class="albumPicLarge"> <img src="'.$source.'"/></div></div>';
                }
            }
        }else
        {
            echo 'Invalid type';
        }

        print_r ($data);

    }
	
	public function getNextFriends()
	{
		$url = $_GET['next'];
	}

    //authenticate new networks
    public function addNetwork(){
        $network = $_GET['network'];

        if ($network =='FLICKR'){
            unset($_SESSION['phpFlickr_auth_token']);
            $this->flickr->authenticate();
        }
    }
	
	//@TODO check if these streams are added by user else ignore.
	public function getUserWall()
	{
        header('Content-Type: application/json');
        //header('Access-Control-Allow-Origin: *');
		
		// Facebook Albums
		$albumList = $this->albums->getFacebookMyAlbums($_SESSION[USER_ID], $_SESSION[USER_TOKEN]);
		foreach($albumList as $a)
		{
			$rec['id'] = $a['id'];
			$rec['title'] = $a['name'];
			$rec['preview'] = $a['cover_large'];
			
			$list[] = $rec;
		}
		
		
		$albumList = array();
		$albumList = $this->albums->getFlickrMyAlbums($_SESSION[FLICKR_USER_ID],$_SESSION[FLICKR_USER_TOKEN],$_SESSION[USER_ID]);
	
		foreach($albumList as $a)
		{
			$rec['id'] = $a['id'];
			$rec['title'] = $a['name'];
			$rec['preview'] = $a['cover_large'];
			
			$list[] = $rec;
		}
		
		echo json_encode($list);
	}


	public function getUserPhotos()
	{
        $list = array();
        $pageNo = $_GET['page'];
		
		$fb = $this->photos->getFacebookMyPhotos($_SESSION[USER_ID],$_SESSION[USER_TOKEN], 0, $pageNo);
		//$fl = $this->photos->getFlickrPhotos($_SESSION[USER_ID], $_SESSION[FLICKR_USER_ID],$_SESSION[FLICKR_USER_TOKEN]);

		$list = $this->photos->getAllPhotos($_SESSION[USER_ID]);
		
		$result['photos'] = $list;
        $result['page']   = $_GET['page'];
        echo json_encode($result);
	}
	
	public function getMyPhotos()
	{	
		$result['photos'] = $list;
        $result['page']   = $_GET['page'];
        echo json_encode($result);
	}
	
	public function getFriendFeed()
	{
        $list = array();
        $pageNo = $_GET['page'];
		
		// Flickr Feed
		//$flickrFeed = $this->feeds->getFlickrFeed($_SESSION[USER_ID],$_SESSION[FLICKR_USER_TOKEN],$pageNo);
				
        // Facebook Feed
		$fbFeed = $this->feeds->getFacebookNewsfeed($_SESSION[USER_ID],$_SESSION[USER_TOKEN], $pageNo);
		
		/*
        if (count($fb_feed)!= 0)
            $list = $list + $fb_feed;

		$instagram_feed = $this->instagram->getUserFeed();
		*/
		//$result['photos'] = $list + $instagram_feed;
		
		if (count($flickrFeed)>0) $list = $list + $flickrFeed;
		if (count($fbFeed)>0) $list = $list + $fbFeed;
		
		$result['photos'] = $list;
        $result['page']   = $_GET['page'];
        echo json_encode($result);
	}
	
	
	
    function test(){
		$user = new User();
		
		//$c = new Contacts();
		//$c->getFacebookFriends($_SESSION[USER_ID], $_SESSION[USER_TOKEN]);
		$user->add(1123,'test','test');
		
    }
	
	public function test1(){
	echo '<pre>';
	//$__SESSION['instagram_access_token'] =null;
	
	//echo $this->instagram->getLoginUrl();
	//https://api.instagram.com/oauth/authorize?client_id=21a36474fa254f52b1282738983420b2&redirect_uri=http://localhost/younify/index.php&scope=basic&response_type=code
	
//$code = 'cd26e248ca824bf094351e1dc99d602a';
	
	 //$this->instagram->setUserParams();
	
	//print_r($_SESSION);
	
	$ret = $this->instagram->getUserFeed();
	print_r($ret);
	}
	
	public function resetCache()
	{
		$cache = new Cache();
		$cache->resetFBCache();
	
	}
	
}
?>
