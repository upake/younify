<?php
require_once(MODEL_PATH.'User.model.php');
/**
 * LoadHome class
 * 
 * This is the controller class for younify
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */
 
 
class Rank extends Controller{

	/**
	* constructor
	*
	*/
	private $nw;
	
	const ranks = 3;
	
	private $templates = array
	(
		"rankSqSmall.tpl",
		"rankSqLarge.tpl"
	);
	
	function __construct() {	
		 parent::__construct();
		 $this->fb = new FacebookConnect();
         $this->flickr = new FlickrConnect();
		 $this->instagram = new InstagramConnect();
		 
		 $this->albums = new Albums();
         $this->photos = new Photos();
         $this->feeds = new Feeds();
    }

	public function getFeed()
	{
        $list = array();
        $pageNo = $_GET['page'];

		$fbFeed = $this->feeds->getFacebookNewsfeed($_SESSION[USER_ID],$_SESSION[USER_TOKEN], $pageNo);
		
		//if (count($flickrFeed)>0) $list = $list + $flickrFeed;
		
		
		if (count($fbFeed)>0) $list = $list + $fbFeed;
		
		//@TODO handle if no stats are available
		//$min = $fbFeed['stats'][0]['min'];
		$max = $fbFeed['stats'][0]['max'];
		
		$wi = ($max)/self::ranks;
		
		
		$smarty = new Smarty;
		$smarty->caching = 0;
		
		$output = $smarty->fetch(Core::getTemplate("twRankSqLarge.tpl"));
		foreach($list as $li)
		{
			$rank = 0;
			$rank = floor(($li['stats']-1) / $wi);
			if (!is_numeric($rank) || $rank<0)
				$rank = 0;	
			$rank++;
			
			
			$smarty->assign("title", Core::wordLimit($li['title'],20));
			$smarty->assign("preview",$li['preview']);
			
			if (empty($li['profile_pic']))
				$smarty->assign("profile_pic",'content/images/facebook_default_profile_pic.png'); //@TODO change this url to a proper one
			else
			
			$smarty->assign("post_id",$li['post_id']);
			$smarty->assign("profile_pic",$li['profile_pic']);
			$smarty->assign("message",$li['message']);
			$smarty->assign("name",$li['name']);
			$smarty->assign("description",$li['description']);
			
			$rand_keys = array_rand($this->templates, 1);
	
			if ($li['type'] == 'link' || $li['type'] == 'video')
				$temp = 'rankWide.tpl';
			else if ($rank==3 || $rank ==2)
				$temp = "rankSqLarge.tpl";
			else
				$temp = "rankSqSmall.tpl";
				
			$output .= $smarty->fetch(Core::getTemplate($temp));
		}
		
		$output .= $this->getTweets();

		echo $output;
	}
	
	//@TODO make this private
	public function getTweets()
	{
		$fbFeed = $this->feeds->getTwitterHomeTimeline($_SESSION[USER_ID], $_SESSION[TW_USER_TOKEN], $_SESSION[TW_USER_SECRET]);
		
		$templevel=0;   

		$newkey=0;

		$grouparr[$templevel]="";
		
		
		foreach ($fbFeed as $key => $val) 
		{
			if ($templevel==$val['uid']){
			 $grouparr[$templevel][$newkey]=$val;
			} else {
			 $grouparr[$val['uid']][$newkey]=$val;
			}
			 $newkey++;       
		}
		
	
		$smarty = new Smarty;
		$smarty->caching = 0;
		
		$elements = "";
		
		foreach($grouparr as $group)
		{
		
			$content = "";
			if (is_array($group))
			{
				foreach ($group as $item)
				{
					$smarty->assign("date",'xxxxxxx');
					$smarty->assign("text",$item['message']);
					$smarty->assign("profile_pic",$item['profile_img']);
					$smarty->assign("name",$item['name']);
					$content .= $smarty->fetch(Core::getTemplate("twContainer.tpl"));
				}
			}
			
			$smarty->assign("content",$content);
			$elements .= $smarty->fetch(Core::getTemplate("twRankSqLarge.tpl"));

		}
		
	
		return $elements;
		
		//print_r($grouparr);
		
		//print_r($fbFeed);
		//Sat Sep 01 21:21:56 +0000 2012
		//echo "Sat Sep 06 21:21:56 +0000 2012 ";
		//echo date("Y-m-d", strtotime("Sat Sep 06 21:21:56 +0000 2012"));

	}
}
?>
