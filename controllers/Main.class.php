<?php
require_once(MODEL_PATH.'User.model.php');

/**
 * LoadHome class
 * 
 * This is the controller class for younify
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */
 
 
class Main extends Controller{

	/**
	* constructor
	*
	*/
	
	private $albums;
	
	function __construct() {	
		
		parent::__construct();
		$this->albums = new Albums(); 
		$this->fb = new FacebookConnect();
        $this->flickr = new FlickrConnect();
		 $this->tw = new TwitterConnect();
	}	

   /**
    * Controller method to show view
    * 
    *                 	 
    */
	
	public function index()
	{
		$smarty = new Smarty;
		//var_dump($this->albums->getFacebookMyAlbums());
		$smarty->caching = 1;
		$user = new User();
		$userInfo = $user->get($_SESSION[USER_ID]);

		$smarty->assign("profilePic", $userInfo['profile_pic']);
		$smarty->assign("userName", $userInfo['name']);

		$smarty->display(Core::getTemplate('header.tpl'));
		$smarty->display(Core::getTemplate('default.tpl'));
		$smarty->display(Core::getTemplate('footer.tpl'));
	}
	
	/*
	*  Fetch the feed by rank and use isotope.
	*
	*
	*/
	public function rank()
	{
		$smarty = new Smarty;
		//var_dump($this->albums->getFacebookMyAlbums());
		$smarty->caching = 0;
		$user = new User();
		$userInfo = $user->get($_SESSION[USER_ID]);

		$smarty->assign("profilePic", $userInfo['profile_pic']);
		$smarty->assign("userName", $userInfo['name']);

		$smarty->display(Core::getTemplate('header.tpl'));
		$smarty->display(Core::getTemplate('rank.tpl'));
		$smarty->display(Core::getTemplate('footer.tpl'));
	}
	
	
	public function me()
	{
		$smarty = new Smarty;
		//var_dump($this->albums->getFacebookMyAlbums());
		$smarty->caching = 1;
		$user = new User();
		$userInfo = $user->get($_SESSION[USER_ID]);

		$smarty->assign("profilePic", $userInfo['profile_pic']);
		$smarty->assign("userName", $userInfo['name']);

		$smarty->display(Core::getTemplate('header.tpl'));
		$smarty->display(Core::getTemplate('mypics.tpl'));
		$smarty->display(Core::getTemplate('footer.tpl'));
	}
	
	public function slider()
	{
		$smarty = new Smarty;

		$pics = $this->fb->getAlbumPhotos($_SESSION[USER_ID], 0, $_SESSION[USER_TOKEN]);
		$list = array();
		foreach($pics as $a)
		{
			$rec['id'] = $a['id'];
			$rec['title'] = $a['name'];
			$rec['src_big'] = $a['src_big'];
			$rec['src_small'] = $a['src_small'];				
			$list[] = $rec;
		}
		$smarty->assign("list", $list);
		$smarty->caching = 1;
		$smarty->display(Core::getTemplate('slider.tpl'));
	}
	
	public function addFeeds()
	{
	
		
		$smarty = new Smarty;
		//$smarty->caching = 1;
		$url = $this->tw->getLoginUrl();
		//echo $url;
		
	echo '<a href="'.$url.'">Twitter</a>';
		//echo '<a href="'.$url.'"</a>";
		
		//$smarty->display(Core::getTemplate('header.tpl'));
		//$smarty->display(Core::getTemplate('addFeeds.tpl'));
	//	$smarty->display(Core::getTemplate('footer.tpl'));	
	
	}
	
	public function test()
	{
	
		$d = DateUtil::getInstance();
		print_r($d->pageNoToDateStart(2));
		print_r($d->pageNoToDateEnd(2));
	}

}
?>
