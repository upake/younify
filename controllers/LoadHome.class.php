<?php

/**
 * LoadHome class
 * 
 * This is the controller class for younify
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */
 
 
class LoadHome extends Controller{
	
	// Gallery object, Flickr, etc.
	private $gallery;
	
	// How many thumbs to show on the page
	private $thumbsPerPage = THUMBS_PER_PAGE;
	
	/**
	* constructor
	*
	*/
	
	function __construct() {
	
		 parent::__construct();
		// $this->gallery = ImageGalleryFactory::getInstance('Flickr');
		// $this->gallery->init(array(FLICKR_API_KEY));
		
	}
	
	
   /**
    * Controller method to show view
    * 
    *                 	 
    */
	function show()
	{
		include(VIEW_PATH."LoadHome.view.php");
	}
	
	
	function search() {
		
		$keywords 			= $_GET['keywords'];
		$page	  			= $_GET[Pagination::URL_CURRENT_PAGE];
		$result				= array();
		$totalCount			= false;
		
		if ($keywords!= NULL) {
			try {
				$result = $this->gallery->doSearch($keywords,$this->thumbsPerPage,$page);
			} catch(Exception $e) {
				$data['error'] 	= "Oops something went wrong, please try again. ";
				$totalCount		= false;
			}
			$pagination = new Pagination($this->gallery->getTotalCount(),$this->thumbsPerPage, $page);

			// Pass our data to the view and render the view
			$totalCount	= $this->gallery->getTotalCount();
			$data['pagination'] = $pagination->getPagination();
		}
		
		$data['totalcount']	= $totalCount;
		$data['result']		= $result;
		$data['keywords']	= $keywords;
		include(VIEW_PATH."ImageSearch.view.php");
	}
	
   /**
    * Controller method to show larger version of image
    * 
    *                 	 
    */
	
	function viewImage() {
		
		$data['image'] = urldecode($_GET['image']);
		$data['title'] = urldecode($_GET['title']);
		include(VIEW_PATH."ImageView.view.php");
	}
}

?>