<?php
/**
 * LoadHome class
 * 
 * This is the controller class for younify
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */

require_once(MODEL_PATH.'User.model.php');
require_once(MODEL_PATH . 'Cache.model.php');
 
class Login extends Controller{

	/**
	* constructor
	*
	*/
	
	function __construct() {
		 parent::__construct();	 
		 $this->fb = new FacebookConnect();
         $this->flickr = new FlickrConnect();
		 $this->instagram = new InstagramConnect();
		 $this->twitter = new TwitterConnect();
		 $this->user = User::getInstance();
	}	

   /**
    * Controller method to show view
    * 
    *                 	 
    */
	function show()
	{
		include(VIEW_PATH."Login.view.php");
	}
	
   /**
    * Controller method to show view
    * 
    *                 	 
    */
	
	public function login()
	{
		//@TODO check if the facebook token is expired	
		// $userId = Auth::getUserCookie();
		// $rec = $this->user->getUserNetwork($userId, 'FB');
		// $this->fb->setAccessToken($rec['auth_token']);
		$url = $this->fb->getLoginUrl();
	
		
		header('Location: '.$url);
		// Facebook key is valid we dont need to redirect to facebook login
/*
		if ($this->fb->isKeysValid()){
			// redirect to main
	        $_SESSION['fb_user_id'] = $userId;
			$_SESSION['access_token'] = $this->fb->getAccessToken();
			header('Location:'.Core::getUrl('Main', 'index'));
		} else {
			$url = $this->fb->getLoginUrl();
			header('Location:'.$url);
		}*/
	}
	
	public function loginRedirect()
	{
		$network = $_GET['network'];

		
		if ($network =='FB')
		{
			
			if (isset($_GET['state'])) {
			
				$_SESSION['logged_in'] 	= true;
				$_SESSION['network']	= $network;
				$_SESSION['fb_state'] 	= $_GET['state'];
                $user_id = $this->fb->getUserId();
                $_SESSION['fb_user_id'] = $user_id;
				$_SESSION['access_token'] = $this->fb->getAccessToken();
				$userInfo = $this->fb->getUserInfo($this->fb->getAccessToken());
                $this->user->add($user_id, $userInfo['name'], "https://graph.facebook.com/".$user_id."/picture");
                $_SESSION['user_networks'] = $this->user->getNetworkIds($user_id);
				Auth::createUserCookies($user_id);
				
				//@TODO try to move this somere else - Contacts:syncContacts
				$contacts = new Contacts();
				$contacts->getFacebookFriends($user_id, $_SESSION['access_token']);
				$contacts->getFacebookPages($user_id, $_SESSION['access_token']);
		
			}
			else
			{
				$_SESSION['logged_in'] 	= false;
				$_SESSION['network'] 	= $network;
				$_SESSION['fb_state'] 	= '';
			}
			

			
			$this->user->addNetwork($_SESSION['fb_user_id'], $network,$_SESSION['fb_user_id'],$this->fb->getAccessToken());
		}
        else if ($network =='FLICKR')
        {
            $nw_id = $this->flickr->setUserParams();
			$this->user->addNetwork($_SESSION['fb_user_id'], $network,$nw_id,$this->flickr->getAccessToken());
        }
		else if ($network =='INSTAGRAM')
		{
			//$nw_id = $this->instagram->setUserParams();
			//$this->user->addNetwork($_SESSION['fb_user_id'], $network,$nw_id,
			//print_r($_GET);
		}
		else if ($network =='TW')
		{
			$params = $this->twitter->setUserParams();
			//@TODO do some validation to see if we have valid params
			$this->user->addNetwork($_SESSION['fb_user_id'], 'TW',$params['user_id'],$params['oauth_token'], $params['oauth_token_secret']);
		}		
		else
		{
			echo 'Invalid entry 1';
		}	

		header('Location:'.Core::getUrl('Main', 'index'));
	}	
	
    public function addNetwork(){
        $network = $_GET['network'];

        if ($network =='FLICKR'){
            unset($_SESSION['phpFlickr_auth_token']);
            $this->flickr->authenticate();
        }
    }
	
	
	//@TODO check the expiry date its not correct
	
	// Sets a cookie with facebook id

	
}
?>
