<?php
require_once(CLASS_PATH.'Db.class.php');

class Cache {

    private  $m_pInstance;
    private  $db;

    public function __construct()
    {
		$this->db = Db::getInstance();
		//$db->connect();
    }

	public function setFBAlbums($user_id, $serial)
	{
		$this->db->query("INSERT INTO cache_fb_albums 
							(user_id, cache, cached) 
							values ({$user_id},'{$serial}',now())");
	}
	
	public function getFBAlbums($user_id)
	{
		$maxCache = CACHE_FB_ALBUM;
		$query = "SELECT * FROM cache_fb_albums where user_id={$user_id} and TIME_TO_SEC(TIMEDIFF(now(), cached))<{$maxCache}";
		$result =  $this->db->query($query);
        if ($result) {
            if($result->num_rows == 0){
				$query = "DELETE FROM cache_fb_albums where user_id={$user_id}";
				$this->db->query($query);
                return false;
            }
			$rec = $result->fetch_assoc();
			return unserialize($rec['cache']);
        } 
			return false;
	}
	
	public function setFlickrAlbums($user_id, $serial)
	{
		$this->db->query("INSERT INTO cache_flickr_albums 
							(user_id, cache, cached) 
							values ({$user_id},'{$serial}',now())");
	}
	
	public function getFlickrAlbums($user_id)
	{
		$maxCache = CACHE_FLICKR_ALBUM;
		$query = "SELECT * FROM cache_flickr_albums where user_id={$user_id} and TIME_TO_SEC(TIMEDIFF(now(), cached))<{$maxCache}";
		$result =  $this->db->query($query);
        if ($result) {
            if($result->num_rows == 0){
				$query = "DELETE FROM cache_flickr_albums where user_id={$user_id}";
				$this->db->query($query);
                return false;
            }
			$rec = $result->fetch_assoc();
			return unserialize($rec['cache']);
        } 
			return false;
	}


	
    public function setFlickrfeed($user_id, $day, $data)
    {

		foreach($data as $rec)
		{
							

			$pid = $this->db->escapeString($rec['id']);
			$uid = $this->db->escapeString($rec['owner']);
			$uname = $this->db->escapeString($rec['ownername']);
			
			$src_s = $this->db->escapeString($rec['url_s']);
			$src_l = $this->db->escapeString($rec['url_l']);
			
			$width_l = $this->db->escapeString($rec['width_l']);
			$height_l = $this->db->escapeString($rec['height_l']);

			$width_s = $this->db->escapeString($rec['width_s']);
			$height_s = $this->db->escapeString($rec['height_s']);

			$title = $this->db->escapeString($rec['title']);
			$description = $this->db->escapeString($rec['description']);
			$views = $this->db->escapeString($rec['views']);
			
			
			$query = "INSERT INTO cache_flickr_feed
								(user_id, day, uname, pid, uid, src_s, src_l, width_s,width_l, height_s, height_l, title, description, views, cached)
								values ({$user_id},'{$day}','{$uname}','{$pid}','{$uid}','{$src_s}','{$src_l}','{$width_s}','{$width_l}','{$height_s}','{$height_l}','{$title}','{$description}',{$views}, now())";
			
			try {
				$this->db->query($query);
			}
				catch(Exception $e){						
					//echo 'Caught exception: ',  $e->getMessage(), "<br>";
			}	
		}
    }	
	
	
    public function setFlickrPhoto($user_id, $data)
    {

		foreach($data as $rec)
		{
							

			$pid = $this->db->escapeString($rec['id']);
			$uid = $this->db->escapeString($rec['owner']);
			$uname = $this->db->escapeString($rec['ownername']);
			
			$src_s = $this->db->escapeString($rec['url_s']);
			$src_l = $this->db->escapeString($rec['url_l']);
			
			$width_l = $this->db->escapeString($rec['width_l']);
			$height_l = $this->db->escapeString($rec['height_l']);

			$width_s = $this->db->escapeString($rec['width_s']);
			$height_s = $this->db->escapeString($rec['height_s']);

			$title = $this->db->escapeString($rec['title']);
			$description = $this->db->escapeString($rec['description']);
			$views = $this->db->escapeString($rec['views']);
			$created = date('Y-m-d',$this->db->escapeString($rec['dateupload']));
			
			$query = "INSERT INTO cache_flickr_photo
								(user_id,  uname, pid, uid, src_s, src_l, width_s,width_l, height_s, height_l, title, description, views, created, cached)
								values ({$user_id}, '{$uname}','{$pid}','{$uid}','{$src_s}','{$src_l}','{$width_s}','{$width_l}','{$height_s}','{$height_l}','{$title}','{$description}',{$views}, '{$created}', now())";
			
			try {
				$this->db->query($query);
			}
				catch(Exception $e){						
					//echo 'Caught exception: ',  $e->getMessage(), "<br>";
			}	
		}
    }	
	
    public function setFBPhotoSrc($user_id, $data)
    {

			foreach($data as $rec)
			{
								
				$pid = $this->db->escapeString($rec['photo_id']);
				$src = $this->db->escapeString($rec['src']);
				$width = $this->db->escapeString($rec['width']);
				$height = $this->db->escapeString($rec['height']);
				
				$query = "INSERT INTO cache_fb_photo_src
									(user_id, pid, src, width, height, cached)
									values ({$user_id},'{$pid}','{$src}','{$width}','{$height}',now())";
				
				try {
					$this->db->query($query);
				}
					catch(Exception $e){						
						//echo 'Caught exception: ',  $e->getMessage(), "<br>";
				}	
			}
    }
	
	//@TODO if profile picture and name not found assign defaults
	
    public function getFBNewsfeed($user_id, $day)
    {
        
		$query = "	SELECT 
						type,
						cache_fb_newsfeed.pid id, 
						min(cache_fb_photo_src.width),
						cache_fb_newsfeed.height height,
						cache_fb_newsfeed.width width,
						if(type='photo',cache_fb_photo_src.src,cache_fb_newsfeed.src)  preview,
						cache_fb_newsfeed.title as title,
						cache_fb_friends.name as name,
						cache_fb_friends.picture as profile_pic,
						cache_fb_newsfeed.day,
						'FB' nw_type
					FROM cache_fb_newsfeed
					LEFT JOIN cache_fb_photo_src ON cache_fb_photo_src.pid = cache_fb_newsfeed.pid
					LEFT JOIN cache_fb_friends ON cache_fb_newsfeed.uid = cache_fb_friends.uid AND cache_fb_newsfeed.user_id = cache_fb_friends.user_id
					where cache_fb_newsfeed.day='{$day}' and 
					cache_fb_newsfeed.user_id = {$user_id}
					group by post_id
					order by likes desc
					limit 30";
	
        $result =  $this->db->query($query);
		return $this->db->fetchAllAssoc();

    }

    public function setFBNewsfeed($user_id, $day, $data)
    {

		foreach($data as $rec)
		{							
			$post_id = $this->db->escapeString($rec['post_id']);
			$aid = $this->db->escapeString($rec['aid']);
			$pid = $this->db->escapeString($rec['pid']);
			$uid = $this->db->escapeString($rec['uid']);
			$src = $this->db->escapeString($rec['preview']);
			$link = $this->db->escapeString($rec['link']);
			$width = $this->db->escapeString($rec['width']);
			$height = $this->db->escapeString($rec['height']);
			$title = $this->db->escapeString($rec['title']);
			$comments = $this->db->escapeString($rec['comments']);
			$likes = $this->db->escapeString($rec['likes']);
			$message = $this->db->escapeString($rec['message']);
			$description = $this->db->escapeString($rec['description']);
			$alt = $this->db->escapeString($rec['alt']);
			$type = $this->db->escapeString($rec['type']);
			$query = "INSERT INTO cache_fb_newsfeed
								(post_id, user_id, day, aid, pid, link, uid, src, width, height, title, likes, comments, message, description, type, cached)
								values ('{$post_id}',{$user_id},'{$day}','{$aid}','{$pid}','{$link}','{$uid}','{$src}','{$width}','{$height}','{$title}', '{$likes}', '{$comments}','{$message}','{$description}','{$type}', now())";
			try {
				$this->db->query($query);
			}
				catch(Exception $e){						
					echo 'Caught exception: ',  $e->getMessage(), "<br>";
			}	
		}
    }

	
    public function getFlickrFeed($user_id, $day)
    {
        
		$query = "	SELECT 
						cache_flickr_feed.pid id, 
						cache_flickr_feed.height_s height,
						cache_flickr_feed.width_s width,
						cache_flickr_feed.src_s preview,
						cache_flickr_feed.title as title,
						cache_flickr_feed.uname as name,
						cache_flickr_feed.day,
						'FLICKR' photo_type
					FROM cache_flickr_feed
					where cache_flickr_feed.day='{$day}' and cache_flickr_feed.user_id = {$user_id}";

        $result =  $this->db->query($query);
		return $this->db->fetchAllAssoc();

    }

    public function getFlickrPhoto($user_id)
    {
        
		$query = "	SELECT 
						cache_flickr_photo.pid id, 
						cache_flickr_photo.height_s height,
						cache_flickr_photo.width_s width,
						cache_flickr_photo.src_s preview,
						cache_flickr_photo.title as title,
						cache_flickr_photo.uname as name,
						cache_flickr_photo.day,
						'FLICKR' photo_type
					FROM cache_flickr_photo
					where cache_flickr_photo.user_id = {$user_id}";

        $result =  $this->db->query($query);
		return $this->db->fetchAllAssoc();

    }
	
    public function setFBAlbumPhotos($user_id, $data)
    {

		foreach($data as $rec)
		{
							
			$aid = $this->db->escapeString($rec['aid']);
			$pid = $this->db->escapeString($rec['pid']);
			$uid = $this->db->escapeString($rec['uid']);
			$src = $this->db->escapeString($rec['preview']);
			$link = $this->db->escapeString($rec['link']);
			$width = $this->db->escapeString($rec['width']);
			$height = $this->db->escapeString($rec['height']);
			$title = $this->db->escapeString($rec['title']);
			$created = $this->db->escapeString($rec['created']);
			
			$query = "INSERT INTO cache_my_photos
								(user_id, aid, pid, link, uid, src, width, height, title, created, cached)
								values ({$user_id},'{$aid}','{$pid}','{$link}','{$uid}','{$src}','{$width}','{$height}','{$title}', '{$created}', now())";
			
			try {
				$this->db->query($query);
			}
				catch(Exception $e){						
					//echo 'Caught exception: ',  $e->getMessage(), "<br>";
			}	
		}
    }

    public function getFBAlbumPhotos($user_id, $albumId)
    {
		
		if ($albumId != 0)
		{
		
			$query = "	SELECT 
							cache_my_photos.pid id, 
							min(cache_fb_photo_src.width),
							cache_my_photos.height height,
							cache_my_photos.width width,
							cache_fb_photo_src.src preview,
							cache_my_photos.title as title,
							cache_fb_friends.name as name,
							cache_my_photos.day,
							'FB' photo_type
						FROM cache_my_photos
						JOIN cache_fb_photo_src ON cache_fb_photo_src.pid = cache_my_photos.pid
						LEFT JOIN cache_fb_friends ON cache_my_photos.uid = cache_fb_friends.uid AND cache_my_photos.user_id = cache_fb_friends.user_id
						where cache_my_photos.user_id = {$user_id} and cache_my_photos.aid = {$albumId}
						group by cache_my_photos.pid";
		} else
		
		{
		
			$query = "	SELECT 
							cache_my_photos.pid id, 
							min(cache_fb_photo_src.width),
							cache_my_photos.height height,
							cache_my_photos.width width,
							cache_fb_photo_src.src preview,
							cache_my_photos.title as title,
							cache_fb_friends.name as name,
							cache_my_photos.day,
							'FB' photo_type
						FROM cache_my_photos
						JOIN cache_fb_photo_src ON cache_fb_photo_src.pid = cache_my_photos.pid
						LEFT JOIN cache_fb_friends ON cache_my_photos.uid = cache_fb_friends.uid AND cache_my_photos.user_id = cache_fb_friends.user_id
						where cache_my_photos.user_id = {$user_id}
						group by cache_my_photos.pid";
		}
        $result =  $this->db->query($query);
		return $this->db->fetchAllAssoc();
    }
	
	// Get facebook album pictures grouped
	// Only do this if album has more than 4 pictures
	// else just return the picture
	
    public function getFBAlbumPhotosGrouped($user_id, $albumId)
    {


		$query = "SELECT aid from cache_my_photos WHERE uid = {$user_id} group by aid";
		$result =  $this->db->query($query);
		$recs = $this->db->fetchAllAssoc();

		foreach($recs as $rec)
		{
			$aid = $rec['aid'];

			$query = "	SELECT 
							cache_my_photos.aid aid,
							cache_my_photos.pid id, 
							min(cache_fb_photo_src.width),
							cache_my_photos.height height,
							cache_my_photos.width width,
							cache_fb_photo_src.src preview,
							cache_my_photos.title as title,
							cache_fb_friends.name as name,
							cache_my_photos.day,
							'FB' photo_type
						FROM cache_my_photos
						JOIN cache_fb_photo_src ON cache_fb_photo_src.pid = cache_my_photos.pid
						LEFT JOIN cache_fb_friends ON cache_my_photos.uid = cache_fb_friends.uid AND cache_my_photos.user_id = cache_fb_friends.user_id
						where cache_my_photos.user_id = {$user_id} and cache_my_photos.aid = '{$aid}'
						group by cache_my_photos.pid";

			$result =  $this->db->query($query);
			$res = $this->db->fetchAllAssoc();
			$album[$aid] = $res;
		}
       
		
    }
	
    public function setFlickrContactPublic($user_id, $pageNo, $data)
    {
        $serial =  $this->db->escapeString (json_encode($data));

        $this->db->query("DELETE FROM cache_flickr_contact_public where user_id={$user_id} and page_no = {$pageNo}");
        $this->db->query("INSERT INTO cache_flickr_contact_public
							(user_id, page_no, cache, cached )
							values ({$user_id}, {$pageNo},'{$serial}',now())");
    }

    public function getFlickrContactPublic($user_id, $pageNo)
    {
        $query = "SELECT * FROM cache_flickr_contact_public where user_id={$user_id} and page_no = {$pageNo}";
        $result =  $this->db->query($query);

        if($result->num_rows == 0){
            return false;
        }
        else{
            $rec = $result->fetch_assoc();
            return json_decode($rec['cache'], true);
        }

    }
	
	public function resetFBCache()
	{
        $query = "DELETE from cache_fb_newsfeed";
        $result =  $this->db->query($query);
		
        $query = "DELETE from cache_fb_photo_src";
        $result =  $this->db->query($query);
		
        $query = "DELETE from cache_my_photos";
        $result =  $this->db->query($query);
		
        $query = "DELETE from cache_my_photos";
        $result =  $this->db->query($query);
		
	}
	

	
	// Get all pictures combined
	
	public function getAllMyPics($userId)
	{
	
		$query1 = "	SELECT 
						cache_my_photos.pid id, 
						min(cache_fb_photo_src.width),
						cache_my_photos.height height,
						cache_my_photos.width width,
						cache_fb_photo_src.src preview,
						cache_my_photos.title as title,
						cache_fb_friends.name as name,
						cache_my_photos.day,
						'FB' photo_type,
						created
					FROM cache_my_photos
					JOIN cache_fb_photo_src ON cache_fb_photo_src.pid = cache_my_photos.pid
					LEFT JOIN cache_fb_friends ON cache_my_photos.uid = cache_fb_friends.uid AND cache_my_photos.user_id = cache_fb_friends.user_id
					where cache_my_photos.user_id = {$userId}
					group by cache_my_photos.pid";
		
		$query2 = "	SELECT 
						cache_flickr_photo.pid id, 
						0,
						cache_flickr_photo.height_s height,
						cache_flickr_photo.width_s width,
						cache_flickr_photo.src_s preview,
						cache_flickr_photo.title as title,
						cache_flickr_photo.uname as name,
						cache_flickr_photo.day,
						'FLICKR' photo_type,
						created
					FROM cache_flickr_photo
					where cache_flickr_photo.user_id = {$userId}";
			
		$query = "SELECT * FROM (".$query1." UNION ".$query2.") a ORDER BY CREATED DESC";
		
        $result =  $this->db->query($query);
		return $this->db->fetchAllAssoc();
		
	}
}
?>