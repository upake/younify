<?php
require_once(CLASS_PATH.'Db.class.php');

class User{
    
	private static $m_pInstance;
    private static $mysqli;
	private $db;
	
    function __construct()
    {
        $this->db = Db::getInstance();
    }

    public function getInstance()
    {
        if (!self::$m_pInstance)
        {
            self::$m_pInstance = new User();
            $this->db = Db::getInstance();
            self::$mysqli = $this->db->connect();
        }

        return self::$m_pInstance;
    }

    public function add($user_id, $name, $profile_pic)
    {
        
		try {
			$query = "SELECT user_id FROM user where user_id={$user_id}";
			$this->db->query($query);
			$result= $this->db->fetchAllAssoc();
			
			if (count($result)==0) {
					$this->db->query("INSERT INTO user (user_id, name, profile_pic) values ({$user_id}, '{$name}', '{$profile_pic}')");
			}
		} catch (Exception $e)		
		{
			// Do nothing
		}
    }
	
    public function get($user_id)
    {
        
		try {
			$query = "SELECT * FROM user where user_id={$user_id}";
			$this->db->query($query);
			$result= $this->db->fetchAllAssoc();
			return $result[0];
		} catch (Exception $e)		
		{
			// Do nothing
		}
    }

    public function addNetwork($user_id, $network, $nw_user_id, $authToken, $authSecret=NULL)
    {
		$query = "SELECT nw_user_id FROM user_network where  user_id={$user_id} and nw_type='{$network}'";
	
		if ($result =  self::$mysqli->query($query)) {

			if($result->num_rows === 0){
				self::$mysqli->query("INSERT INTO user_network (user_id, nw_type, nw_user_id, auth_token, auth_secret) values ({$user_id},'{$network}','{$nw_user_id}','{$authToken}', '{$authSecret}')");
			}
			else
			{
				//@TODO make this an update
				self::$mysqli->query("DELETE FROM user_network where  user_id={$user_id} and nw_type='{$network}'");
				self::$mysqli->query("INSERT INTO user_network (user_id, nw_type, nw_user_id, auth_token, auth_secret) values ({$user_id},'{$network}','{$nw_user_id}','{$authToken}'), '{$authSecret}'");
			}
		}
    }

    public function getNetworkIds($user_id)
    {
        if ($result =  self::$mysqli->query("SELECT nw_type, nw_user_id FROM user_network where  user_id={$user_id}")) {

            if($result->num_rows != 0){
                $arr = array();
                while ($row = $result->fetch_row()) {
                    $arr[$row[0]]= $row[1];
                }
             return $arr;
            }

        }
    }
	
    public function getUserNetwork($user_id, $nw_type)
    {
		$query = "SELECT * FROM user_network where user_id={$user_id} and nw_type='{$nw_type}'";
		$result =  self::$mysqli->query($query);
        if ($result) {
            if($result->num_rows === 0){
                return false;
            }
			return $result->fetch_assoc();
        } 
			return false;
        //$result->close();
    }
}
?>