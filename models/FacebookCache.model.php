<?php
require_once(CLASS_PATH.'Db.class.php');

class FacebookCache {

    private  $m_pInstance;
    private  $db;

    public function __construct()
    {
		$this->db = Db::getInstance();
		//$db->connect();
    }

    public function setFBPages($user_id, $data)
    {
		foreach($data as $rec)
		{
							
			$uid = $this->db->escapeString($rec['page_id']);
			$picture = $this->db->escapeString($rec['pic_small']);
			$name = $this->db->escapeString($rec['name']);
			$query = "INSERT INTO cache_fb_friends
								(user_id, uid, picture, name, type, cached)
								values ({$user_id},'{$uid}','{$picture}','{$name}','page', now())";
			
			try {
				$this->db->query($query);
			}
				catch(Exception $e){						
					//echo 'Caught exception: ',  $e->getMessage(), "<br>";
			}	
		}
    }
	
	
    public function setFBFriends($user_id, $data)
    {
	
		foreach($data as $rec)
		{
							
			$uid = $this->db->escapeString($rec['id']);
			$picture = $this->db->escapeString($rec['picture']['data']['url']);
			$name = $this->db->escapeString($rec['name']);
			$query = "INSERT INTO cache_fb_friends
								(user_id, uid, picture, name, type, cached)
								values ({$user_id},'{$uid}','{$picture}','{$name}','friend',now())";
			
			try {
				$this->db->query($query);
			}
				catch(Exception $e){						
					//echo 'Caught exception: ',  $e->getMessage(), "<br>";
			}	
		}
    }
	
	
	//@TODO if profile picture and name not found assign defaults
	
    public function getFBNewsfeed($user_id, $day)
    {
        
		$limit = 30;
		$query = "	SELECT 
							cache_fb_newsfeed.post_id post_id,
							cache_fb_newsfeed.type,
							cache_fb_newsfeed.pid id, 
							min(cache_fb_photo_src.width),
							cache_fb_newsfeed.height height,
							cache_fb_newsfeed.width width,
							if(cache_fb_newsfeed.type='photo',cache_fb_photo_src.src,cache_fb_newsfeed.src)  preview,
							cache_fb_newsfeed.title as title,
							cache_fb_friends.name as name,
							cache_fb_newsfeed.message as message,
							cache_fb_newsfeed.description as description,
							cache_fb_friends.picture as profile_pic,
							cache_fb_newsfeed.day,
							cache_fb_newsfeed.likes+cache_fb_newsfeed.comments as stats,
							'FB' nw_type
					FROM cache_fb_newsfeed
					LEFT JOIN cache_fb_photo_src ON cache_fb_photo_src.pid = cache_fb_newsfeed.pid
					LEFT JOIN cache_fb_friends ON cache_fb_newsfeed.uid = cache_fb_friends.uid AND cache_fb_newsfeed.user_id = cache_fb_friends.user_id
					where cache_fb_newsfeed.day='{$day}' and 
					cache_fb_newsfeed.user_id = {$user_id}
					group by post_id
					order by likes desc
					limit {$limit}";
					
		$result =  $this->db->query($query);
		return $this->db->fetchAllAssoc();

    }
	
	
    public function getFBNewsFeedStats($user_id, $day)
    {
        
		

		$limit = 30;
		$query = "select min(stats) min, 
						max(stats) max from
					(
					SELECT 
							likes+comments stats
					FROM   cache_fb_newsfeed 
						   LEFT JOIN cache_fb_photo_src 
								  ON cache_fb_photo_src.pid = cache_fb_newsfeed.pid 
						   LEFT JOIN cache_fb_friends 
								  ON cache_fb_newsfeed.uid = cache_fb_friends.uid 
									 AND cache_fb_newsfeed.user_id = cache_fb_friends.user_id 
					WHERE  cache_fb_newsfeed.day = '{$day}' 
						   AND cache_fb_newsfeed.user_id = {$user_id} 
					GROUP  BY post_id
					ORDER  BY likes DESC 
					LIMIT  30 
					) a";

        $result =  $this->db->query($query);
		return $this->db->fetchAllAssoc();

    }

    public function setFBNewsfeed($user_id, $day, $data)
    {

		foreach($data as $rec)
		{							
			$post_id = $this->db->escapeString($rec['post_id']);
			$aid = $this->db->escapeString($rec['aid']);
			$pid = $this->db->escapeString($rec['pid']);
			$uid = $this->db->escapeString($rec['uid']);
			$src = $this->db->escapeString($rec['preview']);
			$link = $this->db->escapeString($rec['link']);
			$width = $this->db->escapeString($rec['width']);
			$height = $this->db->escapeString($rec['height']);
			$title = $this->db->escapeString($rec['title']);
			$comments = $this->db->escapeString($rec['comments']);
			$likes = $this->db->escapeString($rec['likes']);
			$message = $this->db->escapeString($rec['message']);
			$description = $this->db->escapeString($rec['description']);
			$alt = $this->db->escapeString($rec['alt']);
			$type = $this->db->escapeString($rec['type']);
			$query = "INSERT INTO cache_fb_newsfeed
								(post_id, user_id, day, aid, pid, link, uid, src, width, height, title, likes, comments, message, description, type, cached)
								values ('{$post_id}',{$user_id},'{$day}','{$aid}','{$pid}','{$link}','{$uid}','{$src}','{$width}','{$height}','{$title}', '{$likes}', '{$comments}','{$message}','{$description}','{$type}', now())";
			try {
				$this->db->query($query);
			}
				catch(Exception $e){						
					echo 'Caught exception: ',  $e->getMessage(), "<br>";
			}	
		}
    }


}
?>