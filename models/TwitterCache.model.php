<?php
require_once(CLASS_PATH.'Db.class.php');

class TwitterCache {

    private  $m_pInstance;
    private  $db;

    public function __construct()
    {
		$this->db = Db::getInstance();
		//$db->connect();
    }


    public function setTwHomeTimeline($user_id, $day, $data)
    {
		$contacts = array();
		
		foreach($data as $rec)
		{							
			$post_id = $this->db->escapeString($rec['id_str']);			
			$message = $this->db->escapeString($rec['text']);
			$uid = $this->db->escapeString($rec['user']['id_str']);			
			$name = $this->db->escapeString($rec['user']['name']);
			$profile_img = $this->db->escapeString($rec['user']['profile_img']);
			//$hash_tags = $this->db->escapeString($rec['comments']);			
			$hash_tags = NULL;
			$retweet_count			= $this->db->escapeString($rec['retweet_count']);
			$favorited = $this->db->escapeString($rec['favorited']);
			//$day = $this->db->escapeString($rec['alt']);
			$day = NULL;
			
			$created_at = $this->db->escapeString($rec['created_at']);
			
			
			$query = "INSERT INTO cache_twitter_home_timeline
								(post_id, user_id, message, day, uid, name, profile_img, hash_tags, retweet_count, favorited, created_at, cached)
								values ('{$post_id}','{$user_id}','{$message}', '{$day}','{$uid}','{$name}','{$profile_img}','{$hash_tags}','{$retweet_count}', '{$favorited}', '{$created_at}', now())";
			
			
			// Add contacts
			
			if (!in_array($uid, $contacts)) 
			{
				$contacts[] = $uid;
				$this->setTwContact($user_id, $rec['user']);
			}
	
			try {
				$this->db->query($query);
			}
				catch(Exception $e){						
				//	echo 'Caught exception: ',  $e->getMessage(), "<br>";
			}	
		}
    }

    public function setTwContact($user_id, $rec)
    {
						
		$uid = $this->db->escapeString($rec['id_str']);			
		$name = $this->db->escapeString($rec['name']);
		$screen_name = $this->db->escapeString($rec['screen_name']);
		$profile_img = $this->db->escapeString($rec['profile_image_url']);
		$followers = $this->db->escapeString($rec['followers_count']);
		$url = $this->db->escapeString($rec['url']);
		$location = $this->db->escapeString($rec['location']);

		$query = "INSERT INTO cache_twitter_contacts
							(user_id, uid, name, profile_img, screen_name, followers, url, location, cached)
							values ('{$user_id}','{$uid}','{$name}','{$profile_img}','{$screen_name}','{$followers}', '{$url}', '{$location}', now())";

		try {
			$this->db->query($query);
		}
		catch(Exception $e)
		{	
			//@TODO try to use and update if exception is raised from query
			//echo 'Caught exception: ',  $e->getCode(), "<br>";
		}	
		
    }

	
	public function getTwHomeTimeline($user_id, $day)
	{
		$query= "set @num := 0, @uid := 0;"; 

		
		$result =  $this->db->query($query);
			$query= "
				select *, 
					cache_twitter_contacts.name,
					cache_twitter_contacts.profile_img,
					cache_twitter_contacts.url
				from (
				   select uid, 
				   retweet_count,
				   message,
					  @num := if(@uid = uid, @num + 1, 1) as row_number,
					  @uid := uid as dummy
				  from cache_twitter_home_timeline
				  where user_id = '{$user_id}'
				  order by  uid, retweet_count desc
				) as x 
				LEFT JOIN cache_twitter_contacts ON 
					x.uid = cache_twitter_contacts.uid
				where x.row_number <= 5
				
				"; //@TODO change this limit and move it to a config // Add user_id to query
		
		$result =  $this->db->query($query);
		
		return $this->db->fetchAllAssoc();
	}
	
}
?>