<?php
/**
 * Home page
 *
 *
 * @author Hewa W Upake De Silva
 *
 */
// Disable direct access to this view using this constant
if(!defined('VIEWCONST')){die('Direct access not premitted');}

$myLayout='simpleGrid';

if ($_SESSION['logged_in']){
	$login_logout_url ="http://localhost/younify/index.php?c=Api&m=logout";
	$login_logout = 'Logout';
}
else
{
	$login_logout_url =HOST."/younify/index.php?c=Login&m=login";
	$login_logout = 'Login';
}

$me_link = "http://localhost/younify/index.php?c=Api&m=showMyAlbums";
$friends_link = "http://localhost/younify/index.php?c=Api&m=showFriends";
$add_link = HOST.DIR."/index.php?c=Login&m=addNetwork";

?>
 
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>YOUnify - Bringing all my pictures into one place</title>
  <meta name="description" content="Bringing all my pictures into one place">
  <meta name="author" content="Prasadini, Upake">

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- Styling for other elements -->
  <link rel="stylesheet" href="views/css/custom.css">

  <link rel="stylesheet" href="views/css/smoothness/jquery-ui-1.8.20.custom.css">

  <!-- Include the imagesLoaded plug-in -->
  <script src="js/jquery.imagesloaded.js"></script>

</head>

<body>
	<div id="header">
		<div class="logo"><a href="http://www.younify.com"><img src="views/images/logo.jpg" width="28" height="28" alt="younify"></a></div>
		<div class="primary">
			<ul> 
				<li><a href="<?php echo $me_link?>">Me</a></li> 
				<li><a href="<?php echo $friends_link?>">Friends</a></li>  
			 </ul> 
		</div>
		<div class="options"> 
			<ul> 
				<li class="search"> 
					<input type="text" name="search" value="Search" class="">
				</li> 
				<li class="layout dropdown" id="layout">
					<a href="">Layout
						<img src="views/images/small-arrow-down.png" width="10" height="6" alt="">
					</a>
					<div class="ddwrap"> 
						<ul> 
							<li><a id="infinitescroll" href="">Infinite Scroll</a></li> 
							<li><a id="grid" href="">Grid</a></li> 
							<li><a id="album" href="">Album</a></li> 
							<li><a id="tree" href="">Tree</a></li> 
							<li><a id="timeline" href="">Timeline</a></li> 
						</ul>
					</div> 
				</li>
				<li><a href="<?php echo $login_logout_url?>"><?php echo $login_logout?></a></li> 
			 </ul> 
		</div>
		<div class="headerSpacer"></div>
	</div> <!-- End of header -->
    <div id="sidebar">
        <ul>
            <li><a href="<?php echo $add_link.'&network=FLICKR'?>"><img src="views/images/flickr_32.jpg"></a></li>
        </ul>
    </div> <!-- End of sidebar -->
			<div id="home-tiles">
		
			<ul>
				<!-- These are our default images -->
                <li><img src="views/images/image_1.jpg" width="200" height="200"><p>11</p></li>
                <li><img src="views/images/image_2.jpg" width="200" height="300"><p>12</p></li>
                <li><img src="views/images/image_3.jpg" width="200" height="252"><p>13</p></li>
                <li><img src="views/images/image_4.jpg" width="200" height="158"><p>14</p></li>
                <li><img src="views/images/image_5.jpg" width="200" height="300"><p>15</p></li>
                <li><img src="views/images/image_6.jpg" width="200" height="297"><p>16</p></li>
                <li><img src="views/images/image_7.jpg" width="200" height="200"><p>17</p></li>
                <li><img src="views/images/image_8.jpg" width="200" height="200"><p>18</p></li>
                <li><img src="views/images/image_9.jpg" width="200" height="398"><p>19</p></li>
                <li><img src="views/images/image_10.jpg" width="200" height="267"><p>20</p></li>
				<li><img src="views/images/image_1.jpg" width="200" height="200"><p>11</p></li>
				<li><img src="views/images/image_2.jpg" width="200" height="300"><p>12</p></li>
				<li><img src="views/images/image_3.jpg" width="200" height="252"><p>13</p></li>
				<li><img src="views/images/image_4.jpg" width="200" height="158"><p>14</p></li>
				<li><img src="views/images/image_5.jpg" width="200" height="300"><p>15</p></li>
				<li><img src="views/images/image_6.jpg" width="200" height="297"><p>16</p></li>
				<li><img src="views/images/image_7.jpg" width="200" height="200"><p>17</p></li>
				<li><img src="views/images/image_8.jpg" width="200" height="200"><p>18</p></li>
				<li><img src="views/images/image_9.jpg" width="200" height="398"><p>19</p></li>
				<li><img src="views/images/image_10.jpg" width="200" height="267"><p>20</p></li>
				<li><img src="views/images/image_1.jpg" width="200" height="283"><p>21</p></li>
				<li><img src="views/images/image_2.jpg" width="200" height="300"><p>22</p></li>
				<li><img src="views/images/image_3.jpg" width="200" height="252"><p>23</p></li>
				<li><img src="views/images/image_4.jpg" width="200" height="158"><p>24</p></li>
				<li><img src="views/images/image_5.jpg" width="200" height="300"><p>25</p></li>
				<li><img src="views/images/image_6.jpg" width="200" height="297"><p>26</p></li>
				<li><img src="views/images/image_7.jpg" width="200" height="200"><p>27</p></li>
				<li><img src="views/images/image_8.jpg" width="200" height="200"><p>28</p></li>
				<li><img src="views/images/image_9.jpg" width="200" height="398"><p>29</p></li>
				<li><img src="views/images/image_10.jpg" width="200" height="267"><p>30</p></li>
				<!-- End of grid blocks -->
			</ul>
			</div>

<footer id="footer"> Copywright Paxified Corp. 2012</footer>
<!-- Mask to cover the whole screen -->
 <div id="mask"></div>
</div>

  <!-- include jQuery -->
  <script src="views/js/jquery-1.7.2.min.js"></script>
  
  <!-- Include the plug-in -->
  <script src="views/js/jquery.wookmark.js"></script>
  
  <!-- Include the ui plug-in -->
  <script src="views/js/jquery-ui-1.8.20.custom.min.js"></script>


  <!-- Once the page is loaded, initalize the plug-in. -->
  <script type="text/javascript">
    var handler = null;
    
    // Prepare layout options.
    var options = {
      autoResize: true, // This will auto-update the layout when the browser window is resized.
      container: $('#main'), // Optional, used for some extra CSS styling
      offset: 2, // Optional, the distance between grid items
      itemWidth: 210 // Optional, the width of a grid item
    };
    
    /**
     * When scrolled all the way to the bottom, add more tiles.
     */
    function onScroll(event) {
      // Check if we're within 100 pixels of the bottom edge of the broser window.
      var closeToBottom = ($(window).scrollTop() + $(window).height() > $(document).height() - 100);
      if(closeToBottom) {
        // Get the first then items from the grid, clone them, and add them to the bottom of the grid.
        var items = $('#tiles li');
        var firstTen = items.slice(0, 10);
        $('#tiles').append(firstTen.clone());
        
        // Clear our previous layout handler.
        if(handler) handler.wookmarkClear();
        
        // Create a new layout handler.
        handler = $('#tiles li');
        handler.wookmark(options);
      }
    };
	
	function login(){
	
	var $dialog = $('<div id="dialog-login"></div>')
		.html('This dialog will show every time!')
		.dialog({
			autoOpen: false,
			title: 'Basic Dialog',
			close: function(){
				$('#mask').hide();
			},
		});
		
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);
		$dialog.dialog('open');

	}
  
    $(document).ready(new function() {
		// Capture link click event.
		//$('#login').click(function(){
		//	login();
			// prevent the default action, e.g., following a link
		//	return false;
		//});

		//if mask is clicked
		$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
		});		
		

		$( "#layout li a" ).click( function( e ) {
			//redo layout
			
			if (e.currentTarget.id =='infinitescroll')
			{
				//$(document).bind('scroll', onScroll);	
				//handler = $('#tiles li');
				//handler.wookmark(options);				
			}
			else
			{
				// $(document).unbind('scroll');
				$("#tiles li").each(function (i) {
					if (this.style.color != "blue") {
					  this.style.display = "none";
					} else {
					  this.style.color = "";
					}	
				});				
			}


		 }); 			
			
    });


  </script>
</body>
</html>
