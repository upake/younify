<?php
/**
 * Friend Canvas
 *
 *
 * @author Hewa W Upake De Silva
 *
 */
// Disable direct access to this view using this constant
if(!defined('VIEWCONST')){die('Direct access not premitted');}

//echo $nextPage;
$myLayout='simpleGrid';

if ($_SESSION['logged_in']){
	$login_logout_url ="http://localhost/younify/index.php?c=Api&m=logout";
	$login_logout = 'Logout';
}
else
{
	$login_logout_url ="http://localhost/younify/index.php?c=Api&m=login";
	$login_logout = 'Login';
}

$me_link = "http://localhost/younify/index.php?c=Api&m=showMyAlbums";
$friends_link = "http://localhost/younify/index.php?c=Api&m=showFriends";
$album_photos_link ="http://localhost/younify/index.php?c=Api&m=showAlbumPics&albumId=";

?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>younify - Bringing all my pictures into one place</title>
  <meta name="description" content="Bringing all my pictures into one place">
  <meta name="author" content="Prasadini, Upake">

  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="stylesheet" href="views/css/custom.css">
  <link rel="stylesheet" href="views/css/layouts/<?php echo $myLayout?>.css">
</head>

<body>
	<div id="header">
		<div class="logo"><a href="http://www.younify.com"><img src="views/images/logo.jpg" width="28" height="28" alt="younify"></a></div>
		<div class="primary">
			<ul> 
				<li><a href="<?php echo $me_link?>" id="me">Me</a></li> 
				<li><a href="<?php echo $friends_link?>" id="friends">Friends</a></li>  
			 </ul> 
		</div>
		<div class="options"> 
			<ul> 
				<li class="search"> 
					<input type="text" name="search" value="Search" class="">
				</li> 
				<li class="layout dropdown" id="layout">
					<a href="">Layout
						<img src="views/images/small-arrow-down.png" width="10" height="6" alt="">
					</a>
					<div class="ddwrap"> 
						<ul> 
							<li><a id="infinitescroll" href="">Infinite Scroll</a></li> 
							<li><a id="grid" href="">Grid</a></li> 
							<li><a id="album" href="">Album</a></li> 
							<li><a id="tree" href="">Tree</a></li> 
							<li><a id="timeline" href="">Timeline</a></li> 
						</ul>
					</div> 
				</li>
				<li><a href="<?php echo $login_logout_url?>"><?php echo $login_logout?></a></li> 
			 </ul> 
		</div>
	</div> <!-- End of header -->
	<div id="canvas">

		<ul>
			<?php		
			foreach($albumArr as $key=>$value){
				echo '<li>
					 <a href="#" id="'.$value['id'].'" class="show_hide" type="'.$value['album_type'].'" title="'.$value['name'].'">
						<img class="album-cover" src="'.$value['cover_large'].'"/>
                        <div id="img-banner" class="'.$value['album_type'].'">&nbsp;
                        </div>
						<h1>'.$value['name'].'</h1>
					</a>
					</li>';
			}
			?>
		</ul>
	</div>


</div>

<!-- This is the container for the albums - sliding view -->
<div class="slidingDiv">
    <div class="slideClose">
        Close
    </div>
    <img class="albumLeftArrow" src="http://localhost/younify/views/images/left_arrow_16.jpg"/>
    <div class="albumPicStrip">
    </div>
    <img class="albumRightArrow" src="http://localhost/younify/views/images/right_arrow_16.jpg"/>
</div>
<!-- SCRIPTS ---------------------------- -->
<script src="views/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">

    $(document).ready(new function() {
        //// <a href="'.$album_photos_link.$value['id'].'">
        $(".slidingDiv").hide();
        //$(".show_hide").show();

        $('.show_hide').click(function(){
//            alert(this.id + ' fetch album pics ' + this.type);
//            $.each(this, function(key, element) {
//                alert('key: ' + key + '\n' + 'value: ' + element);
//            });
//
//            $.each(this.childNodes, function(key, element) {
//                alert('key: ' + key + '\n' + 'value: ' + element);
//            });

            $(".canvas").hide();
            $.get("http://localhost/younify/index.php?c=Api&m=getAlbumPics", { id: this.id, albumType: this.type, format:"html" },
                function(data){
                   // alert("albm type"+ this.type);
                    //$(".albumPicStrip").append(data);
                    $(".albumPicStrip").append(data);
                    $(".slidingDiv").show();
                });

        });

        //if close is clicked
        $('.slideClose').click(function () {
            $('.slidingDiv').hide();
            $(".canvas").show();
        });
    });

</script>
</body>
</html> 
 