<?php
/**
 * Friend Canvas
 *
 *
 * @author Hewa W Upake De Silva
 *
 */
// Disable direct access to this view using this constant
if(!defined('VIEWCONST')){die('Direct access not premitted');}

//echo $nextPage;
$myLayout='simpleGrid';

if ($_SESSION['logged_in']){
	$login_logout_url ="http://localhost/younify/index.php?c=Api&m=logout";
	$login_logout = 'Logout';
}
else
{
	$login_logout_url ="http://localhost/younify/index.php?c=Api&m=login";
	$login_logout = 'Login';
}

$me_link = "http://localhost/younify/index.php?c=Api&m=showMyAlbums";
$friends_link = "http://localhost/younify/index.php?c=Api&m=showFriends";

?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>younify - Bringing all my pictures into one place</title>
  <meta name="description" content="Bringing all my pictures into one place">
  <meta name="author" content="Prasadini, Upake">

  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="stylesheet" href="views/css/custom.css">
  <link rel="stylesheet" href="views/css/layouts/<?php echo $myLayout?>.css">
</head>

<body>
	<div id="header">
		<div class="logo"><a href="http://www.younify.com"><img src="views/images/logo.jpg" width="28" height="28" alt="younify"></a></div>
		<div class="primary">
			<ul> 
				<li><a href="<?php echo $me_link?>" id="me">Me</a></li> 
				<li><a href="<?php echo $friends_link?>" id="friends">Friends</a></li>  
			 </ul> 
		</div>
		<div class="options"> 
			<ul> 
				<li class="search"> 
					<input type="text" name="search" value="Search" class="">
				</li> 
				<li class="layout dropdown" id="layout">
					<a href="">Layout
						<img src="views/images/small-arrow-down.png" width="10" height="6" alt="">
					</a>
					<div class="ddwrap"> 
						<ul> 
							<li><a id="infinitescroll" href="">Infinite Scroll</a></li> 
							<li><a id="grid" href="">Grid</a></li> 
							<li><a id="album" href="">Album</a></li> 
							<li><a id="tree" href="">Tree</a></li> 
							<li><a id="timeline" href="">Timeline</a></li> 
						</ul>
					</div> 
				</li>
				<li><a href="<?php echo $login_logout_url?>"><?php echo $login_logout?></a></li> 
			 </ul> 
		</div>
	</div> <!-- End of header -->
	
	<div id="canvas">
	
		<ul>
			<?php		
			foreach($photoArr as $key=>$value){
				echo '<li>						
						<img src="'.$value['photo_small'].'"/>
						<div id="img-big"><img src="'.$value['photo_large'].'"/></div>
					  </li>';
			}
			?>
		</ul>
	</div>
</body>
</html> 
 