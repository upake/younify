<!DOCTYPE html>
<html>
<head>
	<title>Fotorama example</title>
	<script src="content/js/jquery-1.7.1.min.js"></script>
	<link rel="stylesheet" href="content/css/fotorama.css">
	<script src="content/js/fotorama.js"></script>
	<script>
		fotoramaDefaults = {
			width: 700,
			height: 467,
			fullscreenIcon: true
		}
	</script>
</head>
<body>

<h1>Fotorama example</h1>

<div class="fotorama" data-width="499" data-height="333">
	{foreach from=$list item=i}
	<a href="{$i.src_big}"><img src="{$i.src_small}"></a>
	{/foreach}
</div>
</body>
</html>