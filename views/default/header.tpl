<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>{$title}</title>
  <meta name="description" content="">
  <meta name="author" content="Upake De Silva">

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- CSS Reset -->
  <link rel="stylesheet" href="content/css/reset.css">
  
  <link rel="stylesheet" href="content/css/core.css">
  
  <!-- Styling for your grid blocks -->

   
  <link rel="stylesheet" href="content/css/style.css">

  <link rel="stylesheet" href="content/css/custom.css">
  
   <link rel="stylesheet" href="http://localhost/younify/content/css/istope.style.css">
 
  <link rel="stylesheet" href="http://localhost/younify/content/css/younify.modal.css">
  
  <!-- <link rel="stylesheet" href="content/css/jquery.ui.all.css"> -->

  <!-- include jQuery -->
  <script src="content/js/jquery-1.7.1.min.js"></script>
  
  <!-- Include the plug-in -->
  <script src="content/js/jquery.wookmark.js"></script>
  
  <!-- Jquery UI
  <script src="content/js/jquery-ui-1.8.20.custom.min.js"></script>
  -->
<script src="content/js/jquery.isotope.min.js"></script>

<script src="content/js/fake-element.js"></script>

</head>
<body>
	<div id="header">
		<div class="logo"><a href="http://www.younify.co"><img src="content/images/logo.png" alt="younify"></a></div>
		<div class="profile">
			<div>
				<img src="{$profilePic}" width="28" height="28" alt="younify">
			</div>
		</div>
		<div class="primary">
			<ul> 
				<li>{$userName}</li> 
				<li><a href="<?php echo $me_link?>">Me</a></li> 
				<li><a href="<?php echo $friends_link?>">Friends</a></li>  
			 </ul> 
		</div>
		<div class="options"> 
			<div class="settings"><a href="http://www.younify.co"><img src="content/images/gear32x32.png" alt="younify"></a></div>
			<div class="login"><a href="?m=Login&c=login"><img src="content/images/loginFacebook.png" alt="younify"></a></div>
		</div>
		<div class="headerSpacer"></div>
	</div> 
	<!-- End of header -->