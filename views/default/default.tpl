<link rel="stylesheet" href="content/css/custom.css">
<div id="container">
    <div id="main" role="main">
        <!-- We place content loaded from the Wookmark API below-->
        <ul id="tiles">

        </ul>

        <div id="loader">
            <div id="loaderCircle"></div>
        </div>


        <div id="mask">
            <div id="maskClose">Close</div>
            <div id="tile-large"></div>
        </div>
    </div>

<footer>
</footer>

  <!-- Mask to cover the whole screen -->

{literal}  
  <!-- Once the page is loaded, initalize the plug-in. -->
  <script type="text/javascript">
    var handler = null;
    var page = 1;
    var isLoading = false;
	// @TODO get proper url
    var apiURL = 'index.php?c=Api&m=getUserPhotos'
    var page_2_html='';
    
    // Prepare layout options.
    var options = {
      autoResize: true, // This will auto-update the layout when the browser window is resized.
      container: $('#tiles'), // Optional, used for some extra CSS styling
      offset: 5, // Optional, the distance between grid items
      itemWidth: 227 // Optional, the width of a grid item
    };
    
    /**
     * When scrolled all the way to the bottom, add more tiles.
     */
    function onScroll(event) {
      // Only check when we're not still waiting for data.
      if(!isLoading) {
        // Check if we're within 100 pixels of the bottom edge of the broser window.
        var closeToBottom = ($(window).scrollTop() + $(window).height() > $(document).height() - 100);
        if(closeToBottom) {
          loadData(page);
        }
      }
    };
    
    /**
     * Refreshes the layout.
     */
    function applyLayout() {
      // Clear our previous layout handler.
      if(handler) handler.wookmarkClear();
      
      // Create a new layout handler.
      handler = $('#tiles li');
      handler.wookmark(options);
    };
    
    /**
     * Loads data from the API.
     */
    function loadData(in_page) 
	{
      isLoading = true;
      $('#loaderCircle').show();

      $.ajax({
        url: apiURL,
        dataType: 'json',
        data: {page: in_page}, // Page parameter to decide which data to load
        success: onLoadData
      });
    };
    
    /**
     * Receives data from the API, creates HTML for images and updates the layout
     */
    function onLoadData(data) {
      isLoading = false;
		$('#loaderCircle').hide();
        // Increment page index for future calls.
        //if (data.page != 1)
            page++;

        if  (data != undefined){

            // Create HTML for the images.
            var html = '';
            var arr = data.photos;
            var i=0, length=arr.length, image;
            for(; i<length; i++) {
            image = arr[i];
            html += '<li>';

            // Image tag (preview in Wookmark are 200px wide, so we calculate the height based on that).
            html += '<img src="'+image.preview+'" width="200" height="'+Math.round(image.height/image.width*200)+'">';

            // Image title.
            html += '<p>'+image.title.substring(0,100)+'..</p>';
			
            html += '<div><img style="float:left" src="content/images/'+image.photo_type+'.png" /><p  style="float:left">'+image.name+'</p></div>';
            html += '</li>';
            }

            // Add image HTML to the page.
            if (data.page ==1) //append to front
            {
                $('#tiles').prepend(html);
           }
            else{
                $('#tiles').append(html);
            }

            // Apply layout.
            applyLayout();

            //Handle the click event of an image
            $('#tiles li').click(function(){
                $('#tiles').hide();
                $('#mask').toggleClass("transparent");
                $('#mask').show();
                $('#tile-large').replaceWith('<div id="tile-large"><img src="'+this.childNodes[0].src+'"></div>');

            });
        }
    };
  
    $(document).ready(new function() {
        $('#mask').hide();
      // Capture scroll event.
      $(document).bind('scroll', onScroll);

      // Load cached data from the API.
      loadData(page);

      // Load new data from the API.
      //loadData(1);

        //if mask is clicked
        $('#mask').click(function () {
            $('#mask').hide();
            $('#tiles').show();
        });



    });
	
  </script>
{/literal}