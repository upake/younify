<div class="headerSpacer">
</div>
<div id="wrap">
	<div class="aboutnav">
		<ul class="primary">
			<li class="active"><a href="#">Streams</a></li>
			<li><a href="#">Account</a></li>
			<li><a href="#">Help</a></li>
		</ul>
		<ul class="secondary">
			<li><a href="http://www.wookmark.com/about/contact">Contact</a></li>
			<li><a href="http://www.wookmark.com/about/api">API</a></li>
			<li><a href="http://www.wookmark.com/about/terms">Terms &amp; Privacy</a></li>
			<li><a href="http://www.wookmark.com/about/copyright">Copyright</a></li>
		</ul>
	</div>
	<div id="extras">
		<div class="section followMe clearfix">
			<h1>Manage Streams</h1>
			<p>
				Use this page to manage your streams
			</p>
			<table>
			<tr>
				<td>
					<div id="facebook32x32">
					</div>
				</td>
				<td>
					Facebook
				</td>
				<td>
				</td>				
			</tr>
			<tr>
				<td>
					<div id="flickr32x32">
					</div>			
				</td>
				<td>
					Flickr
				</td>
				<td>
				</td>				
			</tr>			
			</table>
		</div>
	</div>
</div>