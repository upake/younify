<?php
 
	/**
    * Front controller for our application
    * 
	* This will take a request and take a controller and method as a parameter and redirect the 
	* request to the given controller and method, otherwise it will use the default controller
	* and method
	*
	* @author Hewa W Upake De Silva
	*
    */
	
	// Base path
	define('BASE_PATH',dirname(__FILE__));
	
	// Class path
	define('CLASS_PATH',BASE_PATH.'/'.'classes'.'/');
	
	// This is where the controllers are located
	define('CONTROLLER_PATH', BASE_PATH.'/'.'controllers'.'/');
	
	// This is where the the views are located
	define('VIEW_PATH',BASE_PATH.'/'.'views'.'/');
	
	// This is where the models reside
	define('MODEL_PATH',BASE_PATH.'/'.'models'.'/');
	
	// CSS paths
	define('CSS_PATH',VIEW_PATH.'css'.'/');

	// Default Controller if nothing is specified
	define('DEFAULT_CONTROLLER', 'Main');
	//define('DEFAULT_CONTROLLER', 'Api');
	
	// Default method to run on the controller
	define('DEFAULT_METHOD', 'index');
	//define('DEFAULT_METHOD', 'test1');

	// Include our config
	require_once(BASE_PATH.'/'.'config.php');
	
	require_once(BASE_PATH.'/classes/thirdparty/smarty/libs/Smarty.class.php');
	
	// This line is to resolve conflict with smarty;
	spl_autoload_register('__autoload');
	
	// Autoload our classes from our class path
	function __autoload($class) {
		require BASE_PATH."/classes/".$class.'.class.php';
	}
	

	
	// Create an instance of our controller
	if (!$_GET['c'])
		$controllerName = DEFAULT_CONTROLLER;
	else
		$controllerName = $_GET['c'];
	
	$controllerName = ucfirst($controllerName);
	
	// Load our controller
	if (!file_exists(CONTROLLER_PATH.$controllerName.".class.php")) throw new Exception('Not a valid controller');
	require_once(CONTROLLER_PATH.$controllerName.".class.php");
	
	$controller = new $controllerName();
	
	// Check if the controller implements Controller or throw exception
	if (!($controller instanceof Controller)) throw new Exception('Not a valid controller');
	if (!$_GET['m'])
		$methodName = DEFAULT_METHOD;
	else
		$methodName = $_GET['m'];

	if (!(method_exists($controller, $methodName))) throw new Exception('Not a valid method');
	
	$controller->$methodName();
	
?>