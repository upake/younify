<?php 
require_once('Social.class.php');
require_once("thirdparty/twitter/tmhOAuth.php");
require_once("thirdparty/twitter/tmhUtilities.php");

/**
 * Twitter Connect - Interface for twitter.
 *
 *

 * @author     Upake De Silva
 * @copyright  #############
 */

class TwitterConnect extends Social {
   
   /**
    * Create a Twitter Connect Object
    * 
    *                 	 
    * @return obj
    *    A new Twitter object.	 
    */
    
   function __construct() {   
      parent::__construct('TW');
   }
   
   /**
    * Initialize using the application keys
    * 
    */
    
   protected function init() {

      $this->obj =  new tmhOAuth(array(
        'consumer_key'    => TW_API_KEY,
        'consumer_secret' => TW_SECRET
      ));      
   }
 
   /**
    * Fetch the user tokens and assign them to userParams for DB commit
    * 
    */ 
   
   public function setUserParams() 
   {
		$this->userParams = array();
		
		$this->obj->config['user_token']  = $_SESSION['oauth']['oauth_token'];
		$this->obj->config['user_secret'] = $_SESSION['oauth']['oauth_token_secret'];
		
		$code = $this->obj->request(
		'POST',
		$this->obj->url('oauth/access_token', ''),
		array(
		 'oauth_verifier' => $_REQUEST['oauth_verifier']
		)
		);
		if ($code == 200) {

		//$_SESSION['access_token'] = $this->obj->extract_params($this->obj->response['response']);
		$response = $this->obj->extract_params($this->obj->response['response']);
		$this->userParams[] = $response['oauth_token'];
		$this->userParams[] = $response['oauth_token_secret'];     

		return $response;
		} else {
		 echo "error";
		 $this->error = $this->obj->response['response'];
		 return false;
		}      
   }
   
   /**
    * Implementation to generate login url
    * @return String
    *    Return login a url	
    */ 
    
   public function getLoginUrl() {
     $code = $this->obj->request(
       'POST',
       $this->obj->url('oauth/request_token', ''),
       array(
         'oauth_callback' => $this->loginReturnUri
       )
     );
     $this->response = $this->obj->response['response'];

     if ($code == 200) {
       $_SESSION['oauth'] = $this->obj->extract_params($this->obj->response['response']);
       return $authurl = $this->obj->url("oauth/authorize", '') .  "?oauth_token={$_SESSION['oauth']['oauth_token']}";
     } else {
       $this->error = $this->obj->response['response'];
       return false;
     }
   }
   
   /**
    * Implementation to check if the application and user tokens are still vald
    * @return boolean
    *    Return TRUE if successul or FALSE upon failure
    */ 
              
   public function isKeysValid() {
      $this->obj->config['user_token']   = $this->userParams[0];
      $this->obj->config['user_secret']  = $this->userParams[1];
      $code = $this->obj->request('GET', $this->obj->url('1/account/verify_credentials'));
      if ($code == 200) {
        return true;
      } else {     
        $this->error = $this->obj->response['response'];
        return false;
      }
   }   
   
   /**
    * Implementation to post an update to the network
    * @return boolean
    *    Return TRUE if successul or FALSE upon failure
    */ 
        
   public function doPost($message) {
      $this->obj->config['user_token']   = $this->userParams[0];
      $this->obj->config['user_secret']  = $this->userParams[1];    

      $code = $this->obj->request('POST', $this->obj->url('1/statuses/update'), array(
        'status' => $message
      ));
      $this->response = $this->obj->response['response'];
      if ($code == 200) {
        return true;
      } else { 
        $this->error = $this->obj->response['response'];
        return false;
      }
   }
   
   
   public function getHomeTimeline($userToken, $userSecret)
   {
   
		$this->obj->config['user_token']  = $userToken;
		$this->obj->config['user_secret'] = $userSecret;
		
		$params = array ("count"=>200);
		
		$code = $this->obj->request('GET', $this->obj->url('1/statuses/home_timeline'), $params);
		
		if ($code == 200) 
		{
			$response = $this->obj->response['response'];
			return json_decode($response, true);
		} else 
		{
			//TODO log error
		  tmhUtilities::pr(htmlentities($tmhOAuth->response['response']));
		  return false;
		}
  
   }
   
   
}


?>