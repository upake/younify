<?php

/**
 * Controller class
 * 
 * This class handles functionality related to controllers.
 * All controllers should inherit this class
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */
 
 
class Controller {

	/**
	* constructor
	*
	*/
	

	function __construct() {
		define('VIEWCONST', 1);
		$auth = new Auth();
		$auth->checkAuth();
	}
	
}
?>