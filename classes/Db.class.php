<?php

class Db
{
    private static $m_pInstance;
	private $con;
	
	private $result;
		
		
    private function __construct()
    {
        //do nothing
    }

    public function getInstance()
    {
        if (!self::$m_pInstance)
        {
            self::$m_pInstance = new Db();
			self::$m_pInstance->connect();
        }

        return self::$m_pInstance;
    }

    public function connect()
    {
        $this->con = $mysqli = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB);

        /* check connection */
        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }
        else
        {
            return $this->con;
        }

    }
	
	public function query($query)
	{
					
		$this->result = $this->con->query($query);
		if (!$this->result) {
			throw new Exception($this->con->error.":".$query);
		}
		return true;
	}
	
	public function escapeString($string)
	{
		return $this->con->real_escape_string($string);
	}
	
	public function fetchAllAssoc()
	{
		while ($row = $this->result->fetch_assoc()) {
			$recs[] = $row;
		}		
		$this->result->free();		 
		return $recs;
	}
	
}


?>