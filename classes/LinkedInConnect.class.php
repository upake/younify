<?php 
require_once('Social.class.php');
require_once(APP_ROOT."/classes/linkedin/linkedin_3.2.0.class.php");
/**
 * LinkedIn Connect - Interface for LinkedIn.
 *
 *
 *
 * @author     Upake De Silva 
 * @copyright  ###########
 */

class LinkedInConnect extends Social {
   
   /**
    * Create a LinkedIn Connect Object
    * 
    *                 	 
    * @return obj
    *    A new LinkedIn object.	 
    */
    
   function __construct() {   
      parent::__construct('LI');
   }
   
   /**
    * Initialize using the application keys
    * 
    */

   protected function init() {
      $r = new request();
      $apiConfig = array(
        'appKey'       => LINKEDIN_KEY,
        'appSecret'    => LINKEDIN_SECRET,
        'callbackUrl'  => $this->loginReturnUri
      );
      $this->obj = new LinkedIn($apiConfig);
   }
   
   /**
    * Fetch the user tokens and assign them to userParams for DB commit
    * 
    */
    
   public function setUserParams() {
      try {
      $this->userParams    = array();
      $response = $this->obj->retrieveTokenAccess($_SESSION['oauth']['linkedin']['request']['oauth_token'], $_SESSION['oauth']['linkedin']['request']['oauth_token_secret'], $_GET['oauth_verifier']);

      if($response['success'] === TRUE) {
      
         print_r($response['linkedin']['access']);
         $this->userParams [] = $response['linkedin']['oauth_token'];
         $this->userParams [] = $response['linkedin']['oauth_token_secret'];
         $this->userParams [] = $response['linkedin']['oauth_expires_in'];
         $this->userParams [] = $response['linkedin']['oauth_authorization_expires_in'];
        } else {
          // bad token access
          $this->error = "Access token retrieval failed:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response, TRUE) . "</pre><br /><br />LINKEDIN OBJ:<br /><br /><pre>" . print_r($OBJ_linkedin, TRUE) . "</pre>";
          return false;
        }
      } 
      catch (Exception $e) {
          $this->error = $e;
          return false;
      }
   }
   
   /**
    * Implementation to generate login url
    * @return String
    *    Return login a url	
    */ 
    
   public function getLoginUrl() {
      
      if (!$_GET['oauth_verifier'] && !$_GET['oauth_token']) {
         $response = $this->obj->retrieveTokenRequest(); 
         $this->response = $response;
         
         if($response['success'] === TRUE) {
          // store the request token
            $_SESSION['oauth']['linkedin']['request'] = $response['linkedin'];
            return LINKEDIN::_URL_AUTH . $response['linkedin']['oauth_token'];
         } else {
          // bad token request
            $this->error ='Request token retrieval failed'; 
            return false;
         }    
      } 
      {
         return false;
         $this->error ='Login already requested'; 
      }
   }
   
   /**
    * Implementation to check if the application and user tokens are still valid
    * @return boolean
    *    Return TRUE if successul or FALSE upon failure
    */ 
          
   public function isKeysValid() {
      $access['oauth_token']        = $this->userParams[0];
      $access['oauth_token_secret'] = $this->userParams[1];
      $access['oauth_expires_in']   = $this->userParams[2];
      $access['oauth_authorization_expires_in'] = $this->userParams[3];
      $this->obj->setTokenAccess($access);         
      
      $response = $this->obj->profile('~:(id,first-name,last-name,picture-url)');
      if($response['success'] === TRUE) {
         return true;
      } else {        
        $this->error = "Error retrieving profile information:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response) . "</pre>";
        return false;
      } 
   }   
   
   /**
    * Implementation to post an update to the network
    * @return boolean
    *    Return TRUE if successul or FALSE upon failure
    */ 
    
   public function doPost($message) { 
      try {
         $access['oauth_token']        = $this->userParams[0];
         $access['oauth_token_secret'] = $this->userParams[1];
         $access['oauth_expires_in']   = $this->userParams[2];
         $access['oauth_authorization_expires_in'] = $this->userParams[3];
         $this->obj->setTokenAccess($access);
         $content['comment'] = $message;
         $response = $this->obj->share('new',$content, FALSE);
         if($response['success'] === TRUE) {
            return true;
         } else {        
            $this->error = "Error sharing content:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response, TRUE) . "</pre><br /><br />LINKEDIN OBJ:<br /><br /><pre>" . print_r($OBJ_linkedin, TRUE) . "</pre>";
            return false;
         }    
      } catch (LinkedInException $e) {
         $this->error = $e;
         return false;
      }
   }
}


?>