<?php

/**
 * SocialFactory.class.php - Factory method to generate a social object
 *
 *
 *
 * @author     Upake De Silva <upaked@worldoferp.com>
 * @copyright  ################
 */
 
class SocialFactory {

   /**
    * Method to create an instance of social object
    * 
    * @param String
    *    Name of the class to create an instance of
    *
    * @return obj
    *    A new social object 
    */
    
    public static function getInstance($social)
    {
        if(include_once(dirname(__FILE__).'/classes/'.$social.'.class.php'))
        {
            $class = $social;
            return new  $class;  
        }
        else
        {
            throw new Exception('Class not found');
        }         
    }
}

?>
