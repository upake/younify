<?php
/**
 * Photos.class.php
 *
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */

require_once(MODEL_PATH . 'User.model.php');
require_once(MODEL_PATH . 'Cache.model.php');

class Photos {

    /**
     * constructor
     *
     */

    function __construct() {
        $this->fb = new FacebookConnect();
        $this->flickr = new FlickrConnect();
        $this->cache = new Cache();
        $this->date = DateUtil::getInstance();
    }

    function getFacebookAlbumPhotos($userId, $accessToken, $albumId=0, $pageNo=1)
    {
  
		$this->fb->setAccessToken($accessToken);

        $cache = $this->cache->getFBAlbumPhotos($userId, $albumId);

        if ($cache === false) {

            $result = $this->fb->getAlbumPhotos($userId, $accessToken, $albumId);
            $this->cache->setFBAlbumPhotos($userId, $albumId, $result);

            return $result;
        } else
            return $cache;
    }

	// Fetches all my photos.
	
    function getFacebookMyPhotos($userId, $accessToken, $albumId=0, $pageNo=1)
    {
  
		$result = $this->fb->getAlbumPhotos($userId, $accessToken, $albumId);
		
		$pids = $this->getFacebookPhotosFromFeed($result);
		
		$photoSrc = $this->fb->getPhotoSrc($userId, $accessToken, $pids);
		$this->cache->setFBPhotoSrc($userId, $photoSrc);
		$this->cache->setFBAlbumPhotos($userId, $result);
		
		//$this->cache->getFBAlbumPhotosGrouped($userId, $albumId);
		return $this->cache->getFBAlbumPhotos($userId, $albumId);
		
    }
	
	
	// Get all my photos from cache
	
	function getAllPhotos($userId)
	{
		return $this->cache->getAllMyPics($userId);
	}
	

    function getFlickrContactPhotos($userId, $accessToken, $pageNo=1)
    {
		$this->flickr->getFlickrContactPhotos($accessToken, $pageNo);
    }

    function getFlickrPhotos($userId, $flickrUserId, $accessToken, $pageNo=1)
    {
		$result = $this->flickr->getUserPhotos($accessToken, $flickrUserId);
		$this->cache->setFlickrPhoto($userId, $result);
		return $this->cache->getFlickrPhoto($userId, $day);		
    }
	
	// Extract the PIDs from the feed array
	private function getFacebookPhotosFromFeed($recs)
	{
		foreach($recs as $rec)
		{
			$pid[] = $rec['pid'];
		}
		
		return $pid;
	}
	
}
?>
