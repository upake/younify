<?php 
require_once('Social.class.php');
require_once("thirdparty/facebook/src/facebook.php");
/**
 * Facebook Connect - Interface for facebook
 *
 *
 *
 * @author     Upake De Silva 
 * @copyright  ##############
 */

//https://graph.facebook.com/oauth/access_token?client_id=279356495491255&client_secret=750b6e6d2d488b485919b033841e29bf&grant_type=fb_exchange_token&fb_exchange_token=AAADZBEsCt8LcBAFVy4MsVZCU2EaIkSKmZBCtTAIEzV9Y0cmG0ndf5tLiwWeJ4yfZAGkZBQZArJSqfZAaebNFnpLbO12dpWTNYtlkZChuGFkSawZDZD
class FacebookConnect extends Social {
     
   /**
    * Create a Facebook Connect Object
    * 
    *                 	 
    * @return obj
    *    A new Facebook object.	 
    */
    
   function __construct() {   
      parent::__construct('FB');
   }
   
   /**
    * Initialize using the application keys
    * 
    */
    
   protected function init() {
      $this->obj = new Facebook(array(
        'appId'  => FB_API_KEY,
        'secret' => FB_SECRET,
      ));
 
   }
   
   /**
    * Fetch the user tokens and assign them to userParams for DB commit
    * 
    */
    
   public function setUserParams() {
      $this->userParams    = array();
      $this->userParams[]  = $this->obj->getAccessToken();
   }

   /**
    * Get the access token
    * 
    */
    
    public function getAccessToken() {
		return $this->obj->getAccessToken();
   }
  
	
	public function setAccessToken($token)
	{
		$this->accessToken = $token;
	}
	
	
    public function getUserId() {
        return $this->obj->getUser();
    }
   
   /**
    * Implementation to generate login url
    * @return String
    *    Return login a url	
    */ 
    
      
   public function getLoginUrl() {
   
      try {   
         $options['scope'] = 'user_photos,friends_photos,read_stream,user_likes';
		 // @TODO fix a proper state value here
		 //$options['state'] = '123456';

		//$options['response_type'] = 'token';
		//$options['display'] = 'popup';
		
         if ($this->loginReturnUri)
			$options['redirect_uri'] = $this->loginReturnUri;
          return $this->obj->getLoginUrl($options);

     } catch (FacebookApiException $e) {
         $this->error = $e;
     }       
   }
   
   public function getLogoutUrl() {
   
      try {   
          return $this->obj->getLogoutUrl();
   
     } catch (FacebookApiException $e) {
         $this->error = $e;
     }       
   }   
   /**
    * Implementation to check if the application and user tokens are still vald
    * @return boolean
    *    Return TRUE if successul or FALSE upon failure
    */ 
       
   public function isKeysValid() {
     try {
       // Proceed knowing you have a logged in user who's authenticated.
       $user_profile = $this->obj->api('/me','get',
	   array(
             access_token => $this->accessToken,
			)
		);
       return true;
     } catch (FacebookApiException $e) {
       $this->error = $e;
       return false;
     }      
   }   
   
   /**
    * Get User information
    * @return string
    */ 
   // @TODO make this abstract 
   public function getUserInfo($accessToken) {
     try {
       // Proceed knowing you have a logged in user who's authenticated.
       return $this->obj->api('/me','get',
	   array(
             'access_token' => $accessToken,
			)
		);
		
     } catch (FacebookApiException $e) {
       echo $e;
	   $this->error = $e;
       return false;
     }      
   }  
   
   
   /**
    * Implementation to post an update to the network
    * @return boolean
    *    Return TRUE if successul or FALSE upon failure
    */ 
        
   public function doPost($message) {

    try {
         $this->obj->api("/me/feed", "post", array(
             access_token => $this->userParams[0],
             message => $message
         
      ));  
         return true;      
     } catch (FacebookApiException $e) {
         $this->error = $e;
         return false;
     }      
   }
   
   public function getFriends($accessToken) {
    
		$this->setAccessToken($accessToken);	
		try {
			$response = $this->obj->api("/me/friends", 
											'get', 
											array(
													'fields' =>  array("name","picture"), 
													'access_token' => $this->accessToken
											)); 
	
			return $response['data'];
		 } catch (FacebookApiException $e) {
			 $this->error = $e;
			 echo $e;
			 return false;
		 }      
   }
   
   // Get a list of pages the user has liked
   public function getPages($accessToken)
   {
		try {
		$fql = "SELECT page_id, name, pic_small FROM page WHERE page_id in (select page_id from  page_fan where uid= me())";
		$fql = urlencode($fql);
		$response = $this->obj->api			
		(
			"/fql?q={$fql}",
			array('access_token' => $accessToken, 'format'=>'json-strings')
		);
		return $response['data'];
		} 
		catch (FacebookApiException $e) 
		{
			 $this->error = $e;
			 echo $e;
			 return false;
		}    
   }
   
   
   /*
   SELECT pid, caption, aid, owner, link, src_big, src_small, created, modified FROM photo WHERE pid IN 
   (
SELECT cover_pid FROM album WHERE owner = 655196083
)*/

   public function getAlbums($id) {
		try 
		{
			$fql = urlencode("SELECT object_id, name, size, cover_pid, type, aid FROM album WHERE owner = ".$id);
			$albums = $this->obj->api
						(
							"/fql?q={$fql}",
							array('access_token' => $this->accessToken)
						);

			$fql = urlencode("SELECT aid, src_big, src_small FROM photo WHERE pid IN 
							(SELECT cover_pid FROM album WHERE owner = ".$id.")");
			$covers = $this->obj->api
						(
							"/fql?q={$fql}",
							array('access_token' => $this->accessToken)
						);
			$covers = $covers['data'];
			
			foreach($covers as $cover)
			{
				$temp[$cover['aid']]['src_big'] = $cover['src_big'];
				$temp[$cover['aid']]['src_small'] = $cover['src_small'];
			}

			foreach($albums['data'] as $album)
			{
				
				$a['album_type']    = 'FB_ALBUM';
				$a['id']            = $album['object_id'];
				$a['name']          = $album['name'];
				$a['count']         = $album['size'];
				$a['cover_small'] 	= $temp[$album['aid']]['src_small'];
				$a['cover_large']	= $temp[$album['aid']]['src_big'];
				$albumArr[] = $a;
			}

		return $albumArr;

		}
		catch (FacebookApiException $e) {
			 $this->error = $e;
			 echo $e;
			 return false;
		 }      
   }
   
   
   public function getAlbumPhotos($userId, $accessToken, $albumId)
   {
		
		$this->setAccessToken($accessToken);
		
		try {
				if ($albumId==0)
				{	
					$fql = "SELECT object_id, pid, caption, aid, owner, link, src, src_big, src_small, like_info, comment_info,
										src_small_width,src_small_height,src_big_width,src_big_height,src_width,src_height, created
										FROM photo WHERE aid IN (SELECT aid FROM album WHERE owner = '".$userId."')";
				}
				elseif (is_array($albumId ))
				{
						$fql = "SELECT object_id, pid, caption, aid, owner, link, src, src_big, src_small, like_info, comment_info,
							src_small_width,src_small_height,src_big_width,src_big_height,src_width,src_height, created
							FROM photo WHERE aid IN (".implode(",", $albumId).")";
				}
				else
				{
					$fql = urlencode("SELECT object_id, pid, caption, aid, owner, link, src, src_big, src_small, like_info, comment_info,
										src_small_width,src_small_height,src_big_width,src_big_height,src_width,src_height, created
										FROM photo WHERE aid ='".$albumId."'");
				}
				
				error_log($fql);
				
				$fql = urlencode($fql);
			
			$response = $this->obj->api
						(
							"/fql?q={$fql}",
							array('access_token' => $this->accessToken, 'format'=>'json-strings')
						);
			
			
			foreach($response['data'] as $photo)
			{
				$a['photo_type']        = 'FB';
				$a['pid']               = $photo['object_id'];
				$a['aid']               = $photo['aid'];
				$a['uid']               = $photo['owner'];
				$a['title']             = $photo['caption'];
				$a['preview']		    = $photo['src_big'];
				$a['height'] 	        = $photo['src_big_height'];
				$a['width'] 	        = $photo['src_big_width'];
				$a['likes'] 			= $photo['like_info']['like_count'];
				$a['comments']			= $photo['comment_info']['comment_count'];
				$a['link']  			= $photo['link'];
				$a['created']  			= date('Y-m-d',$photo['created']);
				$picArray[] = $a;
			}

			return $picArray;
		}
		catch (FacebookApiException $e) {
		 $this->error = $e;
		 echo $e;
		 return false;
		}
   }

    /* Recent photos are retrieved on a weekly basis. From Monday-Monday
     * These are cached according to the week no of the year. Ex: 42nd week
     * Younify API will request pages 0 - 52 where 0 will correspond to the current week
     * and 52nd week will correspond to one year ago.
     *
     * */
    public function getRecentPhotos($userId, $accessToken,  $fromDate, $toDate)
    {
        $this->setAccessToken($accessToken);

        try {
            $fql = urlencode("SELECT pid, caption, aid, owner, link, src, src_big, src_small,
                                    src_small_width,src_small_height,src_big_width,src_big_height,src_width,src_height
                                  FROM photo WHERE aid IN (SELECT aid FROM album WHERE owner IN (SELECT uid2 FROM friend WHERE uid1 = me()))
                                  and modified > ".$fromDate." and modified < ".$toDate." limit ". FB_RECENT_PHOTO_LIMIT);

            $response = $this->obj->api
            (
                "/fql?q={$fql}",
                array('access_token' => $this->accessToken)
            );

            foreach($response['data'] as $photo)
            {
                $a['photo_type']        = 'FB';
                $a['id']                = $photo['pid'];
                $a['title']             = $photo['caption'];
                $a['preview']		    = $photo['src_big'];
                $a['height'] 	        = $photo['src_big_height'];
                $a['width'] 	        = $photo['src_big_width'];
                $picArray[] = $a;
            }
            return $picArray;
        }
        catch (FacebookApiException $e) {
            $this->error = $e;
            echo $e;
            return false;
        }
    }
	

    public function getNewsfeed($userId, $accessToken,  $fromDate, $toDate)
    {
        $this->setAccessToken($accessToken);
        $picArray = array();
        try 
		{
			// and type in (247, 65)
			$fql = "SELECT 
						post_id,
						likes.count, 
						comments.count, 
						actor_id, 
						attachment.caption, 
						attachment.media,
						attachment.name,
						attachment.description,
						message						
					FROM 
						stream 
					WHERE 
						filter_key ='nf' AND 
						attachment.media !='' AND
                        updated_time > ".$fromDate."AND updated_time < ".$toDate." limit ". FB_RECENT_PHOTO_LIMIT;

			$fql = urlencode($fql);						  

			$response = $this->obj->api
            (
                "/fql?q={$fql}",
                array('access_token' => $this->accessToken, 'format'=>'json-strings')
            );	
			
            foreach($response['data'] as $attach_arr)
            {
              
				$post_id = $attach_arr['post_id'];
				$caption = $attach_arr['attachment']['caption'];
				$uid = $attach_arr['actor_id'];
				$likes = $attach_arr['likes']['count'];
				$comments =  $attach_arr['comments']['count'];
				$description = $attach_arr['attachment']['description'];
				$name	= $attach_arr['attachment']['name'];
				$message	= $attach_arr['message'];
				
				foreach($attach_arr['attachment']['media'] as $media)
				{
					
					$link = $media['href']; // link to the original post on fb
					//@TODO reset this array
					$a = array();
					$a['post_id'] = $post_id;
					$a['type'] 	= $media['type'];
					$a['message'] = $message;
					$a['description'] = $description;
					$a['comments']	= $comments;
					$a['likes'] 	= $likes;
					$a['link']  = $link;
					$a['alt']	= $media['alt'];
					$a['photo_type']        = 'FB';
					if ($media['type']=='photo'){
						$src_s = $media['src'];
						$src_n = substr($src_s, 0, -5).'n.jpg';
						$a['pid']               = $media['photo']['fbid'];
						$a['aid']               = $media['photo']['aid'];
						$a['uid']               = $uid;
						$a['title']             = $caption;
						$a['preview']		    = $src_n;
						$a['height'] 	        = $media['photo']['height'];
						$a['width'] 	        = $media['photo']['width'];
						
					} else if ($media['type']=='link')
					{
						$src_s = $media['src'];
						$a['uid']               = $uid;
						$a['title']             = $name;
						$a['preview']		    = $media['src'];
					} else if ($media['type']=='video')
					{
						$src_s = $media['src'];
						$a['uid']               = $uid;
						$a['title']             = $name;
						$a['preview']		    = $media['src'];
						
					}
					
					$feedArray[] = $a;
				}
            }
			
			//echo "<pre>";
			//print_r($feedArray);
			//die();
			return $feedArray;
	
        }		
        catch (FacebookApiException $e) {
            $this->error = $e;
            echo $e;
            return false;
        }
    }
	
	// Get specific photo sizes
	
    public function getPhotoSrc($userId, $accessToken,$photos)
    {
        $this->setAccessToken($accessToken);
		
		if (empty($photos))
			return array();			
		$picList = implode(',', $photos);
		
		$picArray = array();
		
        try 
		{
			$fql = "SELECT photo_id, src, width, height FROM photo_src WHERE photo_id in (".$picList.") AND width > 300 and width <500";

			$fql = urlencode($fql);
			
			$response = $this->obj->api
            (
                "/fql?q={$fql}",
                array('access_token' => $this->accessToken)
            );			
			return $response['data'];
			
        }		
        catch (FacebookApiException $e) {
            $this->error = $e;
            echo $e;
            return false;
        }
    }
	
   public function getPhoto($id)
   {
	try {	
        $photo = $this->obj->api($id,'get', array(
			fields => array("picture", "source","height", "width")
		));    
		
       	return  ($photo);
     } catch (FacebookApiException $e) {
         $this->error = $e;
		 echo $e;
         return false;
     }     
   
   }
   
  /*
  SELECT pid, caption, aid, owner, link, src, src_big, src_small, created, modified 
FROM photo WHERE aid IN (SELECT aid FROM album WHERE owner IN (SELECT uid2 FROM friend WHERE uid1 = me())) limit 10

  SELECT post_id, actor_id, target_id, message, type FROM stream WHERE filter_key in (SELECT filter_key FROM stream_filter WHERE uid = me() AND type = 'newsfeed')

SELECT  actor_id, type,attachment, post_id,type FROM stream WHERE filter_key in (SELECT filter_key FROM stream_filter WHERE uid = me() AND type = 'newsfeed')


  SELECT attachment FROM stream WHERE filter_key ='nf' and attachment.media!='' limit 10
  
  {
  "friends":"SELECT uid2 FROM friend WHERE uid1=me()",
  "friend_names":"SELECT uid, name FROM user WHERE uid IN (select uid2 from #friends)"
}

  */
  
}


?>