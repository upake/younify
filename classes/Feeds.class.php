<?php

/**
 * Feeds.class.php
 *
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */

require_once(MODEL_PATH . 'User.model.php');
require_once(MODEL_PATH . 'Cache.model.php');
require_once(MODEL_PATH . 'FacebookCache.model.php');
require_once(MODEL_PATH . 'TwitterCache.model.php');


class Feeds {

    /**
     * constructor
     *
     */

    function __construct() {
        $this->fb = new FacebookConnect();
        $this->flickr = new FlickrConnect();
        $this->cache = new Cache();
		$this->fbcache = new FacebookCache();
        $this->date = DateUtil::getInstance();
    }

    function getFacebookNewsfeed($userId, $accessToken, $pageNo=1)
    {
		
		$this->fb->setAccessToken($accessToken);
		
		$fromDate =strtotime( $this->date->pageNoToDateStart($pageNo));
		$toDate =strtotime( $this->date->pageNoToDateEnd($pageNo));
		
		$day = $this->date->pageNoToDateStart($pageNo);

		$return = $this->fbcache->getFBNewsfeed($userId, $day);
		
		if (count($return)<1)
		{
			$result = $this->fb->getNewsfeed($userId, $accessToken,  $fromDate, $toDate);          
			
			// Extract Albums and Photo IDs
			$pids = $this->getFacebookPhotosFromFeed($result);			
			$aids = $this->getFacebookAlbumsFromFeed($result);
			
			// Cache Photos
			$photoSrc = $this->fb->getPhotoSrc($userId, $accessToken, $pids);
			//$albums = $this->fb->getAlbumPhotos($userId, $accessToken, $aids);
			
			//print_r($albums);
			
			$this->cache->setFBPhotoSrc($userId, $photoSrc);			
			$this->fbcache->setFBNewsfeed($userId, $day, $result);			
		}  
		else
			$return = $this->fbcache->getFBNewsfeed($userId, $day);
		$return['stats'] = $this->fbcache->getFBNewsFeedStats($userId, $day);
		return $return;
    }
	
	
	
    function getFlickrFeed($userId, $accessToken, $pageNo=1)
    {
		$fromDate = $this->date->pageNoToDateStart($pageNo)->getTimestamp();
		$toDate = $this->date->pageNoToDateEnd($pageNo)->getTimestamp();
		$day = $this->date->pageNoToDateStart($pageNo)->format('Y-m-d');	
			
		if ($pageNo==1) //should get fresh data for page 1
		{			
			$result = $this->flickr->getSearchPhoto($accessToken, $fromDate, $toDate);
			$this->cache->setFlickrfeed($userId, $day, $result);
			return $this->cache->getFlickrFeed($userId, $day);
		} else
		{
			
			$return = $this->cache->getFlickrFeed($userId, $day);
			if (count($return)<1)
			{				
				$result = $this->flickr->getSearchPhoto($accessToken, $fromDate, $toDate);
				$this->cache->setFlickrfeed($userId, $day, $result);			
			} else
				return $return;
		}
		return $this->cache->getFlickrFeed($userId, $day);		
    }
	
	
	function getTwitterHomeTimeline($userId, $userToken, $userSecret, $pageNo=1)
	{
		//@TODO implement caching
		$day = $this->date->pageNoToDateStart($pageNo);
		$twitter = new TwitterConnect();
		$result = $twitter->getHomeTimeline($userToken, $userSecret);
		
		$twCache = new TwitterCache();
		$twCache->setTwHomeTimeline($userId, $day, $result);
		
		return $twCache->getTwHomeTimeline($userId, $day);
		
	}
	
	// Extract the PIDs from the feed array
	private function getFacebookPhotosFromFeed($recs)
	{
		foreach($recs as $rec)
		{
			if (!empty($rec['pid']))
				$pid[] = $rec['pid'];
		}
		return $pid;
	}
	
	// Extract the AIDs from the feed array
	private function getFacebookAlbumsFromFeed($recs)
	{
		foreach($recs as $rec)
		{
			if (!empty($rec['aid']))
				$aid[] = "'{$rec['aid']}'";
		}
		return $aid;
	}
	
	
}

?>