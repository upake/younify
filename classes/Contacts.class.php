<?php

/**
 * Feeds.class.php
 *
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */

require_once(MODEL_PATH . 'User.model.php');
require_once(MODEL_PATH . 'Cache.model.php');
require_once(MODEL_PATH . 'FacebookCache.model.php');

class Contacts {

    /**
     * constructor
     *
     */

    function __construct() {
        $this->fb = new FacebookConnect();
        $this->flickr = new FlickrConnect();
        $this->cache = new Cache();
		$this->fbcache = new FacebookCache();
        $this->date = DateUtil::getInstance();
    }

    // Get all the facebook friends of the uesr
	
	function getFacebookFriends($userId, $accessToken)
    {
		$results = $this->fb->getFriends($accessToken);
		$this->fbcache->setFBFriends($userId, $results);
    }
	
	// Get all the facebook pages the user has liked
	
    function getFacebookPages($userId, $accessToken)
    {
		$results = $this->fb->getPages($accessToken);
		$this->fbcache->setFBPages($userId, $results);	
    }
	
}

?>