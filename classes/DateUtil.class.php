<?php
class DateUtil{

    private static $m_pInstance;

    private function __construct()
    {
        //do nothing
    }

    public function getInstance()
    {
        if (!self::$m_pInstance)
        {
            self::$m_pInstance = new DateUtil();
        }

        return self::$m_pInstance;
    }

    public function getCurrentWeek()
    {
        return date("W", time());
    }

    public function getWeekNo($pageNo)
    {
        $cur_week = $this->getCurrentWeek();
        $cur_year = date("Y", time());

        $weekNo =   $cur_week - ($pageNo - 1); //paging starts from PAGE 1
        if ($pageNo > $cur_week) // we need previous years
        {
            $prior_weeks = $pageNo - $cur_week;
            $yearDiff = ceil($prior_weeks/52);
            $year = $cur_year - $yearDiff;
            $prior_weeks = $prior_weeks -1;
            $rem_weeks = 52- ($prior_weeks % 52);
            $ret= $year.'_'.$rem_weeks;

        }
        else{
            $ret = $cur_year.'_'.$weekNo;
        }

        return $ret;

    }

    function getWeekStart($week){
        $predefinedYear = substr($week, 0, 4);
        $predefinedWeeks = substr($week, 5);

        // find first mоnday of the year
        $firstMon = strtotime("mon jan {$predefinedYear}");

        // calculate how much weeks to add
        $weeksOffset = $predefinedWeeks - date('W', $firstMon);

        // calculate searched monday
        $searchedMon = strtotime("+{$weeksOffset} week " . date('Y-m-d', $firstMon));

        return $searchedMon;
    }

    function getWeekFinish($fromDate)
    {
        return ($fromDate + (7 * 24 * 60 * 60));//7 days
    }
	
	// Relate page no to start date
	
	function pageNoToDateStart($page)
	{
		$page--;
		//$interval = new DateInterval('P'.$page.'D');
		//@TODO make this backward compatiable
		//$date = new DateTime(date('d-m-Y'));
		$date = date('Y-m-d');
		//$date->sub($interval);
		
		return $date;
	}

	
	function pageNoToDateEnd($page)
	{
		$date = date('Y-m-d');
		$newdate = strtotime ( '1 day' , strtotime ( $date ) ) ;
		return date ( 'Y-m-d' , $newdate );
		
		
		//if ($page == 1)
		//	return new DateTime("now");	
	
		//$date= $this->pageNoToDateStart($page);
		//@TODO make this backward compatiable
		//$interval = new DateInterval('PT11H59M59S');
		//$date->add($interval);

	}
	
}
?>