<?php 
require_once('Social.class.php');
require_once("thirdparty/flickr/phpFlickr.php");
/**
 * Flickr Connect - Interface for Flickr
 *
 *
 *
 * @author     Upake De Silva 
 * @copyright  ##############
 */

class FlickrConnect extends Social {
   
   /**
    * Create a Flickr Connect Object
    * 
    *                 	 
    * @return obj
    *    A new Flickr object.	 
    */
    
   function __construct() {   
      parent::__construct('FLICKR');
   }
   
   /**
    * Initialize using the application keys
    * 
    */
    
   protected function init() {
      $this->obj = new phpFlickr(FLICKR_API_KEY,FLICKR_SECRET);

   }
   
   //@TODO change url here
   public function authenticate()   {
       //echo '----------'.$this->loginReturnUri;
      //$this->obj->auth($this->loginReturnUri);
       $this->obj->auth(HOST.DIR.'/index.php?c=Login&m=loginRedirect&network=FLICKR');
   }
   /*
   public function getAlbums($id, $albumArr, $last_key=0) {
    try {
	    //flickr has sets, collections, favourites and galleries!
        //check http://www.flickr.com/services/api/misc.urls.html for urls

        $sets = $this->obj->call("flickr.photosets.getList",
            array('user_id' => $id
            ));

        foreach($sets['photosets']['photoset'] as $key => $arr)
        {

            $albumArr[$key + $last_key]['album_type']   = 'FLICKR_PHOTOSET';
            $albumArr[$key + $last_key]['id']           = $arr['id'];
            $albumArr[$key + $last_key]['name']         = $arr['title'];
            $albumArr[$key + $last_key]['count']        = $arr['photos'];
            $albumArr[$key + $last_key]['cover_small']  = "http://farm{$arr['farm']}.staticflickr.com/{$arr['server']}/{$arr['primary']}_{$arr['secret']}_t.jpg";
            $albumArr[$key + $last_key]['cover_large']  = "http://farm{$arr['farm']}.staticflickr.com/{$arr['server']}/{$arr['primary']}_{$arr['secret']}.jpg";
        }
        $last_key = $key + $last_key + 1;

        //galleries are collections of other ppl's photos
        $galleries = $this->obj->call("flickr.galleries.getList",
        	array('user_id' => $id, 'per_page' => $per_page, 'page' => $page
        ));

        foreach($galleries['galleries']['gallery'] as $key => $arr)
        {

            $albumArr[$key + $last_key]['album_type']   = 'FLICKR_GALLERY';
            $albumArr[$key + $last_key]['id']           = $arr['id'];
            $albumArr[$key + $last_key]['name']         = $arr['title'];
            $albumArr[$key + $last_key]['count']        = $arr['count_photos'];
            $albumArr[$key + $last_key]['cover_small']  = "http://farm{$arr['primary_photo_farm']}.staticflickr.com/{$arr['primary_photo_server']}/{$arr['primary_photo_id']}_{$arr['primary_photo_secret']}_t.jpg";
            $albumArr[$key + $last_key]['cover_large']  = "http://farm{$arr['primary_photo_farm']}.staticflickr.com/{$arr['primary_photo_server']}/{$arr['primary_photo_id']}_{$arr['primary_photo_secret']}.jpg";
       }


        //collections are a collection of sets

        //favourites are a list of photos
        //add entry for favourites
//        $key  = $key + $last_key + 1;
//        $albumArr[$key]['album_type']   = 'FLICKR_FAVOURITES';
//        $albumArr[$key]['id']           = $arr['id'];
//        $albumArr[$key]['name']         = $arr['title'];

       	return $key + $last_key;

     } catch (Exception $e) {
         $this->error = $e;
		 echo $e;
         return false;
     }      
   }*/
   
   public function getAlbums($id) {
    try {
	    //flickr has sets, collections, favourites and galleries!
        //check http://www.flickr.com/services/api/misc.urls.html for urls

        $sets = $this->obj->call("flickr.photosets.getList",
            array('user_id' => $id
            ));

        foreach($sets['photosets']['photoset'] as $key => $arr)
        {

            $albumArr[$key + $last_key]['album_type']   = 'FLICKR_PHOTOSET';
            $albumArr[$key + $last_key]['id']           = $arr['id'];
            $albumArr[$key + $last_key]['name']         = $arr['title'];
            $albumArr[$key + $last_key]['count']        = $arr['photos'];
            $albumArr[$key + $last_key]['cover_small']  = "http://farm{$arr['farm']}.staticflickr.com/{$arr['server']}/{$arr['primary']}_{$arr['secret']}_t.jpg";
            $albumArr[$key + $last_key]['cover_large']  = "http://farm{$arr['farm']}.staticflickr.com/{$arr['server']}/{$arr['primary']}_{$arr['secret']}.jpg";
        }
        $last_key = $key + $last_key + 1;

        //galleries are collections of other ppl's photos
        $galleries = $this->obj->call("flickr.galleries.getList",
        	array('user_id' => $id, 'per_page' => $per_page, 'page' => $page
        ));

        foreach($galleries['galleries']['gallery'] as $key => $arr)
        {

            $albumArr[$key + $last_key]['album_type']   = 'FLICKR_GALLERY';
            $albumArr[$key + $last_key]['id']           = $arr['id'];
            $albumArr[$key + $last_key]['name']         = $arr['title'];
            $albumArr[$key + $last_key]['count']        = $arr['count_photos'];
            $albumArr[$key + $last_key]['cover_small']  = "http://farm{$arr['primary_photo_farm']}.staticflickr.com/{$arr['primary_photo_server']}/{$arr['primary_photo_id']}_{$arr['primary_photo_secret']}_t.jpg";
            $albumArr[$key + $last_key]['cover_large']  = "http://farm{$arr['primary_photo_farm']}.staticflickr.com/{$arr['primary_photo_server']}/{$arr['primary_photo_id']}_{$arr['primary_photo_secret']}.jpg";
       }
		
		return $albumArr;
		
     } catch (Exception $e) {
         $this->error = $e;
		 echo $e;
         return false;
     }      
   }
   
    public function getPhotosetPhotos($id, $format='json')
    {
        try {
            $photos = $this->obj->call('flickr.photosets.getPhotos',
                array('photoset_id' => $id, 'extras' => $extras, 'privacy_filter' => $privacy_filter, 'per_page' => $per_page, 'page' => $page, 'media' => $media
                ));

            if ($format =='json')
                return  json_encode($photos);
            else
                return ($photos);
        } catch (Exception $e) {
            $this->error = $e;
            echo $e;
            return false;
        }
    }
   
   public function getGalleryPhotos($id, $format='json')
   {
	try {	
        $photos = $this->obj->call('flickr.galleries.getPhotos', 
			array('gallery_id' => $id, 'extras' => $extras, 'per_page' => $per_page, 'page' => $page
        ));

        if ($format =='json')
            return  json_encode($photos);
        else
            return ($photos);
     } catch (Exception $e) {
         $this->error = $e;
		 echo $e;
         return false;
     }       
   }

    public function getFlickrContactPublic($flickrId,  $userId, $pageNo)
    {

        try{

            $response = $this->obj->call("flickr.photos.getContactsPublicPhotos",
            array('user_id' => $flickrId,  'page' => $pageNo, 'include_self'=>1, 'extras'=>'url_l'
            ));
			
			//print_r($flickrId);
			//print_r($userId);
			//print_r($pageNo);
			
            foreach($response['photos']['photo'] as $photo)
            {
                $a['photo_type']        = 'FLICKR';
                $a['id']                = $photo['id'];
                $a['title']             = $photo['title'];
                $a['preview']		    = $photo['url_l'];
                $a['height'] 	        = $photo['height_l'];
                $a['width'] 	        = $photo['width_l'];
                $picArray[] = $a;
            }

        return $picArray;
        }
        catch (Exception $e) {
            $this->error = $e;
            echo $e;
            return false;
        }
    }

	
    public function getFlickrContactPhotos($token, $pageNo)
    {

        $this->setAccessToken($token);
		
		try{

            $response = $this->obj->call("flickr.photos.getContactsPhotos",
            array('page' => $pageNo, 'include_self'=>1, 'extras'=>'url_l'));
			
			//echo "<pre>";
			
			//print_r($response);
			
			//print_r($flickrId);
			//print_r($userId);
			//print_r($pageNo);
			
            foreach($response['photos']['photo'] as $photo)
            {
                $a['photo_type']        = 'FLICKR';
                $a['id']                = $photo['id'];
                $a['title']             = $photo['title'];
                $a['preview']		    = $photo['url_l'];
                $a['height'] 	        = $photo['height_l'];
                $a['width'] 	        = $photo['width_l'];
                $picArray[] = $a;
            }

        return $picArray;
        }
        catch (Exception $e) {
            $this->error = $e;
            echo $e;
            return false;
        }
    }

	
    public function getSearchPhoto($token,  $fromDate, $toDate)
    {

        $this->setAccessToken($token);
		
		try{

            $response = $this->obj->call("flickr.photos.search",
            array('contacts'=>'all', 'min_taken_date' => $fromDate, 'max_taken_date' => $toDate, 'extras'=>'url_l, url_s, owner_name, views, favorites, description, date_uploaded'));	
			return $response['photos']['photo'];
        }
        catch (Exception $e) {
            $this->error = $e;
            echo $e;
            return false;
        }
    }	

    public function getUserPhotos($token,  $userId)
    {

        $this->setAccessToken($token);
		try{

            $response = $this->obj->call("flickr.photos.search",
            array('user_id'=>$userId, 'extras'=>'url_l, url_s, owner_name, views, favorites, description, date_upload'));			
			return $response['photos']['photo'];
        }
        catch (Exception $e) {
            $this->error = $e;
            echo $e;
            return false;
        }
    }	
	
   public function getPhoto($id)
   {
	try {	
        $photo = $this->obj->call('flickr.photos.getInfo',
			array('photo_id' => $id
        ));

		
       	return  json_encode($photo);      
     } catch (Exception $e) {
         $this->error = $e;
		 echo $e;
         return false;
     }     
   
   }

    public function setUserParams() {
        if (!(empty($_GET['frob']))) {
            $resp = $this->obj->auth_getToken($_GET['frob']);
			$this->accessToken = $resp['token'];
			return $resp['user']['nsid'];
        }
    }

    public function getLoginUrl() {

    }

    public function isKeysValid() {

    }

    public function doPost($message){

    }

	public function setAccessToken($token)
	{
		$this->obj->setToken($token);
		$this->accessToken = $token;
       // $_SESSION['phpFlickr_auth_token'] = $token;

	}
      
    public function getAccessToken() {
		return $this->accessToken;
   }
}


?>