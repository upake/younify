<?php
/**
 * Core.class.php
 * 
 * This is the controller class for younify
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */
 
class Core {

	//@TODO need to implement params to add to url
	
	public static function getUrl($controller, $method, $params=array())
	{
		$url = HOST.DIR."/index.php?c=".$controller."&m=".$method;//."/?".http_build_query($params);
		//$url = HOST.DIR."/".$controller."/".$method."/?".http_build_query($params);
		return $url;
	}
	
	public static function getTemplate($template)
	{
		return VIEW_PATH."/".TEMPLATE."/".$template;
	}
	
	// Limit string and return words
	
	public static function wordLimit($string, $length = 50, $ellipsis = "...") 
	{ 
	   $words = explode(' ', $string); 
	   if (count($words) > $length) 
		   return implode(' ', array_slice($words, 0, $length)) . $ellipsis; 
	   else 
		   return $string; 
	} 
}
?>
