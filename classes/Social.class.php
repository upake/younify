<?php 

/**
 * Social.class.php - Abstract class for social network integration
 *
 *
 *
 * @author     Upake De Silva
 * @copyright  #############
 */

abstract class Social {
   
   // Social network object created using the relavant SDK
   private     $obj;
   
   // Callback URL
   protected   $loginReturnUri;
   
   // Database record    
   private     $dbRec;
   
   // ID of the social network as defined in the DB
   protected   $id;
   
   // Oracle package class associated with handling Social Network
   protected   $pkgClass = "ISV_SCL_NW_EXT";
   
   // User access keys
   protected   $userParams = array();
   
   // Rowdate for concurrency on the DB
   protected   $rowDate;
   
   // This will have the error if any error occurs.
   protected   $error;
   
   // Response from the relevent social network as intepreted by the SDK
   protected   $response;
   
   // Access Token
   protected $accessToken;
   
   /**
    * Create a Social network object and retrieve the database rec.
    * 
    * @param String $id
    * @return obj
    *    A new Social Object
    */
    
   function __construct($network) {     
      $this->loginReturnUri = HOST.DIR."/index.php?c=Login&m=loginRedirect&network=".$network; 
	  //$this->loginReturnUri = Core::getUrl("Login","loginRedirect", array("network"=>$network));
      $this->init();
   }
   
   /**
    * Abstract method to initialize the class, 
    * Ideally this should contain the relavent intitaliztion
    * of the social network object using the application keys
    */
       
   abstract protected function init();
   
   
   /**
    * Abstract method to initalize the user access tokens
    *
    */
    
   abstract public function setUserParams();
   
   /**
    * Generate a login url to fetch the required tokens from the social network
    *
    */    
   abstract public function getLoginUrl();
   
    /**
    * Check if the application keys and user keys are valid
    *
    */    
   abstract public function isKeysValid();   
   
    /**
    * Make a status update on the social network
    *
    */    
   abstract public function doPost($message);
   
   
   /**
    * Method to get the current object for the social network
    * created by the SDK for the relavant network. 
    *
    * @return object
    *    Returns the current object created by the SDK
    */
    
   public function getObj() {
      return $this->obj;
   }
   
   
   /**
    * Method to access our error string
    *
    * @return String
    */
    
   public function getError() {
      return $this->error;
   }
   
    /**
    * Revoke access for the particualr account and remove the keys from the DB.
    *
    */
   public function revoke() {
      $attr['SCL_NW_ID']   = $this->id;
      $attr['ROWDATE']     = $this->rowDate;
      $pkg                 = new pkg($this->pkgClass);
      $rec                 = $pkg->RemoveRec($info, $attr);   

      if ($pkg->isSuccess())
         $this->rowDate = $rec[0]['ROWDATE'];
      return $pkg->isSuccess;   
   }
   
    /**
    * Check if this is a login redirect
    *
    * @return boolean
    */   
   public function isRedirect() {
      
      if ($_GET['social']==$this->id) {
         return true;
      }      
      return false;
   }
   
   public function getUserParams() {	
		return $this->userParams();
   }
}


?>