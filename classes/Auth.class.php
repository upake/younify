<?php

require_once(MODEL_PATH.'User.model.php');

/**
 * Core.class.php
 * 
 * This is the controller class for younify
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */
 
class Auth {
	
	function __construct()
	{

	}
	
	public function checkAuth()
	{
		$fb = new FacebookConnect();
		$user = User::getInstance();
		$userId = Auth::getUserCookie();

		$rec = $user->getUserNetwork($userId, 'FB');
		// Facebook key is valid we dont need to redirect to facebook login
		
		//@TODO restructure this in a proper way
		
		$_SESSION[USER_ID] = $userId;
		$_SESSION[USER_TOKEN] = $rec['auth_token'];
		
		//@TODO see if these feeds are added first
		
		if (!isset($_SESSION[FLICKR_USER_ID]) || !isset($_SESSION[FLICKR_USER_TOKEN]))
		{
			$rec = $user->getUserNetwork($userId, 'FLICKR');
			$_SESSION[FLICKR_USER_ID] = $rec['nw_user_id'];
			$_SESSION[FLICKR_USER_TOKEN] = $rec['auth_token'];
		}
		
		if (!isset($_SESSION[TW_USER_ID]) || !isset($_SESSION[TW_USER_TOKEN]) || !isset($_SESSION[TW_USER_SECRET]))
		{
			$rec = $user->getUserNetwork($userId, 'TW');
			if ($rec)
			{
				$_SESSION[TW_USER_ID] = $rec['nw_user_id'];
				$_SESSION[TW_USER_TOKEN] = $rec['auth_token'];
				$_SESSION[TW_USER_SECRET] = $rec['auth_secret'];				
				$addedFeeds = $_SESSION[ADDED_FEEDS];
				if (!in_array('TWITTER', $addedFeeds))
				{
					$addedFeeds[] = 'TWITTER';
				}
				$_SESSION[ADDED_FEEDS] = $addedFeeds;
			}
		} else
		{
			$addedFeeds = $_SESSION[ADDED_FEEDS];
			if (!in_array('TWITTER', $addedFeeds))
			{
				$addedFeeds[] = 'TWITTER';
			}
			$_SESSION[ADDED_FEEDS] = $addedFeeds;	
		}
	}
	
	public static function createUserCookies($id)
	{
		setcookie(TRACKCOOKIE, base64_encode($id), time()+TRACKCOOKIEEXPIRY);
	}
	

	public static function getUserCookie()
	{
		$userId = $_COOKIE[TRACKCOOKIE];
		return base64_decode($userId);
	}
}
?>
