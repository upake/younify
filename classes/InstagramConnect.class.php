<?php
require_once('Social.class.php');
require_once("thirdparty/instagram/instagram.class.php");

class InstagramConnect extends Social {


	function __construct() {   
      parent::__construct('INSTAGRAM');
    }
   
    protected function init() {
      $this->obj = new Instagram(array(
        'apiKey'  => INSTAGRAM_API_KEY,
        'apiSecret' => INSTAGRAM_SECRET,
		'apiCallback' => 'http://localhost/younify/index.php'
      ));
 
   }
   
   public function setUserParams() {
	  $code = $_GET['code'];
      $this->userParams    = array();
	  $token  = $this->obj->getOAuthToken($code, false);
      $this->userParams[]  = $token;
	  $this->setAccessToken($token);
   }
   
   public function getLoginUrl() {
		return $this->obj->getLoginUrl();
    }

    public function isKeysValid() {

    }

    public function doPost($message){

    }
	
	public function setAccessToken($token)
	{
		$this->obj->setAccessToken($token);
		$this->accessToken = $token;
        $_SESSION[INSTAGRAM_USER_TOKEN] = $token;
	}
   
	public function getUserFeed()
	{
		$this->obj->setAccessToken($_SESSION[INSTAGRAM_USER_TOKEN]);
		$arr = $this->obj->getUserFeed();
		$picArray = array();
		
		foreach($arr->data as $obj){
			if ($obj->type =='image'){
				$a['photo_type']        = 'INSTAGRAM';
				$a['id']                = $obj->id;
				$a['title']             = $obj->caption->text;
				$a['preview']		    = $obj->images->standard_resolution->url;
				$a['height'] 	        = $obj->images->standard_resolution->height;
				$a['width'] 	        = $obj->images->standard_resolution->width;
				
				$picArray[] = $a;
			}
		}
		
		return $picArray;
		
	}
}
?>