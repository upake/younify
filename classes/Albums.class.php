<?php
/**
 * Albums.class.php
 * 
 * This is the controller class for younify
 *
 * @author Hewa W Upake De Silva
 * @version 1.0
 *
 */

require_once(MODEL_PATH.'User.model.php');
require_once(MODEL_PATH.'Cache.model.php');
 
class Albums {

	/**
	* constructor
	*
	*/
	
	function __construct() {
		 $this->fb = new FacebookConnect();
         $this->flickr = new FlickrConnect();
		 $this->cache = new Cache();
	}	

	//@TODO fetch proper tokens and id
	//@TODO if no proper token or user is not found then throw error
	function getFacebookMyAlbums($userId,$accessToken)
	{
		$this->fb->setAccessToken($accessToken);
		$cache = $this->cache->getFBAlbums($userId);
		if (!$cache) {
			$result = $this->fb->getAlbums($userId);
			$this->cache->setFBAlbums($userId, serialize($result));
			return $result;
		} else
			return $cache;
	}
	
	function getFacebookFriendsAlbums()
	{
	
	}
	
	function getFlickrMyAlbums($flickrUserId,$accessToken, $userId)
	{
		$this->flickr->setAccessToken($accessToken);
		$cache = $this->cache->getFlickrAlbums($userId);

		if (!$cache) {
			$result = $this->flickr->getAlbums($flickrUserId);
			$this->cache->setFlickrAlbums($userId, serialize($result));
			return $result;
		} else
			return $cache;
	}
	
	function getFlickrFriendsAlbums()
	{
	
	}
	
	// $albumId by default is zero to fetch all
	function getFacebookMyPhotos($userId,$accessToken, $albumId=0)
	{
		$this->fb->getAlbumPhotos($userId,$accessToken,$albumId);
	}
}
?>
