<?php

/**
* Configuration Options
*
* This is the configuration file
*
* @author Upake De Silva
*
*/

	// Facebook API Keys

	define('FB_API_KEY', '402987543098567');
	define('FB_SECRET', 'b764df92767a4bfb03b86597a12c3d5d');		
	
	// Twitter Keys
	define('TW_API_KEY', 'THmSpnvG9NdVnAoLk8uQ');
	define('TW_SECRET', 'MjcwuFNMoC7X6oDbTSQlahFyLsin9A6BtOu0LW2paU');			
		
	// Flickr API related keys
	define('FLICKR_API_KEY', 'cd3b8daf0028948803fb64994ccd39b4');
	define('FLICKR_SECRET', 'f4988099144ed0a3');
	
	// Instagram API related keys
	define('INSTAGRAM_API_KEY', '21a36474fa254f52b1282738983420b2');
	define('INSTAGRAM_SECRET', 'fc77310dfff6443dab7000f0d43ab633');	

	// How many thumbs to show on the page
	define('THUMBS_PER_PAGE', 5);

	// How many recent photos to retrieve per week
	define('FB_RECENT_PHOTO_LIMIT', 100);
	
	// only fetch from cache, dont make any api calls
	define('ONLY_FROM_CACHE', false);

	define('HOST', 'http://127.0.0.1');
	define('DIR', '/younify');
	define('TRACKCOOKIE', '__praa');
	define('TRACKCOOKIEEXPIRY', 5184000);

	// Cache Settings
	define('CACHING', TRUE); 
	define('CACHE_FB_ALBUM', 1000);
	define('CACHE_FLICKR_ALBUM', 1000); 

	// Template

	define('TEMPLATE', 'default');

	// Session Parameters

	define('USER_ID', 'fb_user_id');
	define('USER_TOKEN', 'access_token');

	define('FLICKR_USER_ID', 'flickr_user_id');
	define('FLICKR_USER_TOKEN', 'phpFlickr_auth_token');
	
	define('TW_USER_ID', 'twitter_user_id');
	define('TW_USER_TOKEN', 'twitter_user_token');
	define('TW_USER_SECRET', 'twitter_user_secret');
	
	define('ADDED_FEEDS', 'added_feeds');
	
	define('INSTAGRAM_USER_TOKEN', 'instagram_access_token');

	// Mysql Connection parameters
	define('MYSQL_HOST', 'localhost');
	define('MYSQL_USER', 'root');
	define('MYSQL_PASSWORD', '');
	define('MYSQL_DB', 'younify');

?>